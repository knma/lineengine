precision highp float;
uniform vec3 uLinesColor;
uniform float uOpacity;
varying float vLen;
varying float vOp;

void main() {

  gl_FragColor.rgb = vec3(1.);

  gl_FragColor.a = vLen * pow(vOp + 0.2, 2.);

  gl_FragColor.a *= uOpacity;
}