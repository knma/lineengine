precision highp float;
uniform sampler2D uPosTex1;
uniform sampler2D uOffsetTex;
uniform float uThickness;
uniform float uAnimFactor;
uniform vec3 uCameraPos;
uniform vec2 uPointer;
uniform float uPointerXScreen;
uniform float uPointerPower;
uniform float uDissolve;
uniform float uDissolving;
attribute float offset_x;
varying vec3 vPosition;
varying vec4 vOffsetColor;
varying float vDiversity;
varying float vW;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
attribute vec3 position;

void main() {

  vec3 pos = position;
  vec3 offsetData = texture2D(uOffsetTex, vec2(offset_x, 0.0)).xyz;

  float width = position.x * (3. + offsetData.x * 1.) * uThickness;

  vW = step(0.001, position.x);

  pos.x = offset_x;

  vec3 toCamera = uCameraPos - pos;
  vec3 toY = vec3(0, 1, 0);
  vec3 orto = normalize(cross(toY, toCamera));

  vec4 vdata = texture2D(uPosTex1, pos.xy);
  vPosition = vdata.rgb;

  float pointerDist = length(uPointer.x - pos.x) * 30.;
  pointerDist = min(pointerDist, 3.1415);
  float pointerFactor = cos(pointerDist) * 0.5 + 0.5;
  width *= 1. + (sin(pos.x * 1000.) * 0.5 + 0.5) *  pointerFactor * uPointerPower * (1. - uAnimFactor);

  float dissolveWidth = position.x * 2. * (1. - step(0.98, offset_x));
  width = mix(width, dissolveWidth, pow(uDissolve, 0.5));
  
  vOffsetColor.a = 1.;
  vOffsetColor.a = pow(1. - pos.y, 1.);
  vOffsetColor.a = smoothstep(0., 1., pos.y  * 11.) * vOffsetColor.a;
  vOffsetColor.a *= min(1.55 - offsetData.x, 1.);
  vDiversity = offsetData.x;

  float op = offsetData.x;
  float dissolveVal = clamp(-1. + op + uDissolve * 2., 0., 1.) ;

  vPosition += orto * width;


  gl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );
}