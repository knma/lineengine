precision highp float;

uniform sampler2D uEdgeTex;
uniform sampler2D uEdgeInitialTex;
uniform sampler2D uEdgeDataTex;
uniform sampler2D uPositionTex;
uniform vec2 uResolution;
uniform float uEdgeFboSide;
uniform float uvStepAdd;

varying vec2 vUv;

void main() {
  vec2 uv = vUv;

  vec2 pixelPos = floor(gl_FragCoord.xy);
  uv = pixelPos / uResolution;
  float uvstep = pow(uResolution.y, -1.) * 2.;

  vec2 edgeUV;
  edgeUV.x = mod(gl_FragCoord.x, uEdgeFboSide);
  edgeUV.x /= uEdgeFboSide;
  edgeUV.y = floor(gl_FragCoord.x / uEdgeFboSide);
  edgeUV.y /= uEdgeFboSide;

  vec4 data = texture2D(uEdgeDataTex, edgeUV);
  float speed = data.z;

  vec4 lastPos = texture2D(uPositionTex, vec2(uv.x, 1.));
  vec4 initialPos = texture2D(uEdgeInitialTex, edgeUV);

  if (lastPos.a > data.x) {
    gl_FragColor = initialPos;
    return;
  }

  if (uv.y == 0.) {
    gl_FragColor = texture2D(uEdgeTex, edgeUV);
    return;
  }

  vec3 prevPos = texture2D(uPositionTex, uv - vec2(0., uvstep)).rgb;

  gl_FragColor.rgb = prevPos;
  gl_FragColor.a = texture2D(uPositionTex, uv).a;

  if (length(gl_FragColor.xyz - initialPos.xyz) > 0.001) {
    gl_FragColor.a += data.y * speed;
  }
}