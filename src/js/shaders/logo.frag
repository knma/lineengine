uniform float uNoise;
uniform float uNoiseAngle;
uniform float uDrawAlpha;
uniform vec3 uColor1;
uniform vec3 uColor2;
uniform float k;
uniform sampler2D uMaskTex;
uniform sampler2D uTransitionTex;

varying vec2 vUv;

void main() {
  vec4 mask = texture2D(uMaskTex, vUv);

  float angle = 1. + uNoiseAngle;
  float scale  = 1.;

  vec2 uv = vUv;
  uv -= 0.5;
  uv.x += uNoise * 0.5;
  uv.x = uv.x * cos(angle) - uv.y * sin(angle);
  uv.x *= scale;
  uv.x += 0.5;
  uv.y = 0.0;
  vec3 col = mix(uColor1, uColor2, uv.x);

  float trans = texture2D(uTransitionTex, vUv).r;
  float a = -1. + trans + k * 2.;
  a = clamp(a, 0., 1.);

  gl_FragColor.a = mask.a * a;
  gl_FragColor.rgb = col;
}