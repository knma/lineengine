precision highp float;

uniform sampler2D uTex;

void main() {
  float r = texture2D(uTex, gl_PointCoord.xy).r;
  gl_FragColor = vec4(r);
  gl_FragColor.a = 1.;
}