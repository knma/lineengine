precision highp float;

uniform sampler2D uEdgeTex;
uniform sampler2D uEdgeInitialTex;
uniform sampler2D uEdgeDataTex;
uniform sampler2D uNoiseTex;
uniform sampler2D uPositionTex;
uniform sampler2D uVertTex1;
// uniform sampler2D uVertTex2;
uniform vec2 uEdgeResolution;
uniform vec2 uResolution;
uniform float uTime;
uniform float uZNoise;
uniform float uNoiseAmount;

varying vec2 vUv;

void main() {
  vec2 uv = vUv;
  vec2 pixelPos = floor(gl_FragCoord.xy);
  uv = pixelPos / uEdgeResolution;

  vec4 data = texture2D(uEdgeDataTex, uv);
  float speed = data.z;

  float posX;
  posX = pixelPos.y * uEdgeResolution.x + pixelPos.x;
  posX /= uResolution.x;

  vec4 lastPos = texture2D(uPositionTex, vec2(posX, 1.));

  vec4 prevData = texture2D(uEdgeTex, uv);

  vec4 initialPos = texture2D(uEdgeInitialTex, uv);

  vec3 pos = prevData.rgb;

  float noise = texture2D(uNoiseTex, pos.xy).r - 0.5;

  float finalMixPower = 0.1;
  float mixPower = clamp(uTime * finalMixPower * 0.3, 0., finalMixPower);
  float mixK = mixPower / finalMixPower;
  mixK = pow(mixK, 2.5);

  vec2 dv;
  float angle = (noise) * 3.4;
  dv.x = sin(angle);
  dv.y = cos(angle);
  dv = mix(dv, vec2(0., 1.), 1. - uNoiseAmount);

  vec3 dpos = normalize(vec3(dv.x, dv.y, 0.0));

  pos += dpos * pow(uResolution.y, -1.) * speed * 0.9;

  pos.x = mix(pos.x, initialPos.x, pow(0.5 - abs(pos.y - 0.47), 1.7) * finalMixPower);

  pos.y = 1. - pos.y + 0.0005;
  pos.x += 0.001;

  pos = texture2D(uVertTex1, pos.xy).rgb;

  float dz = ((sin(posX * 1000.) * 0.5 + 0.5) * (cos(pos.y * 19. + 100. * sin(posX * 15.)) * 0.5 + 0.5)) * 0.12;

  dz = mix(dz, noise * 0.2, 0.3);

  pos.z += uZNoise * dz ;
  

  gl_FragColor.rgb = pos;
  gl_FragColor.a = prevData.a + data.y * speed;

  if (lastPos.a > data.x) {
    gl_FragColor = initialPos;
  } 
}