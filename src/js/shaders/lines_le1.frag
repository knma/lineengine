precision highp float;
uniform sampler2D uMaskTex1;
uniform sampler2D uMaskTex2;
uniform sampler2D uNoiseTex;
uniform float uTime;
uniform float uSurrounding;
uniform float uOpacity;
varying vec3 vPosition;
varying vec4 vOffsetColor;
// varying vec3 vCol;
varying float vW;
// varying float vDiversity;
void main() {
  float mask = texture2D(uMaskTex1, vPosition.xz).r;
  gl_FragColor = vOffsetColor;
  gl_FragColor.a *= mask * (1. - vW * 0.25) * uOpacity;
}