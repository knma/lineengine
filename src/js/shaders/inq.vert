precision highp float;
uniform float uTime;
attribute float offset_x;
attribute float offset_l;
varying float vLen;
varying float vOp;

void main() {

  vec3 pos = position;

  vLen = offset_l;

  vec3 dpos = vec3(0.);
  vec3 dpos2;
  float t = uTime * 0.75;

  float off = pow(offset_x, 0.3) * 2.7;
  float _dk = .17 + sin(uTime * 0.5) * 0.1;
  dpos.x = sin(t + off) * _dk;
  dpos.y = sin(t * 0.7 + off) * _dk;
  dpos.z = sin(t * 0.4 + off) * _dk;
  float dk = 0.015;
  dpos2.x = sin(offset_x * 4000.);
  dpos2.y = sin(offset_x * 1000.);
  dpos2.z = sin(offset_x * 300.);

  pos += dpos;
  pos += dpos2 * dk;

  vOp = sin(offset_x * 100000. + uTime * 0.8) * 0.5 + 0.5;

  vOp *= cos(offset_x * 20000. + offset_l * 6. + uTime * 2.) * 0.5 + 0.5;

  gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
  
}