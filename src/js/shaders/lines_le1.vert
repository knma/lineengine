precision highp float;
uniform sampler2D uVertTex1;
// uniform sampler2D uVertTex2;
uniform sampler2D uNormTex1;
// uniform sampler2D uNormTex2;
uniform sampler2D uOffsetTex;
uniform sampler2D uNoiseTex;
// uniform sampler2D uTransitionTex;
uniform float uTime;
uniform float k;
uniform float uThickness;
uniform float uZeroZ1;
uniform float uZeroZ2;
uniform float uOneZ1;
uniform float uOneZ2;
uniform float uMultiplier;
uniform float uLightIntensity;
uniform vec3 uLightDir;
uniform float uPointerX;
uniform float uMovement;
uniform float uLineLength;
uniform float uDiversity;
attribute float offset_x;
varying vec3 vPosition;
varying vec4 vOffsetColor;
// varying float vK;
varying float vW;
// varying vec3 vCol;
// varying float vDiversity;
uniform float uTwinkleMultiplier;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
attribute vec3 position;
void main(){
  vec3 pos = position;
  vec3 offsetData = texture2D(uOffsetTex, vec2(offset_x, 0.0)).xyz;


  // float n = abs(texture2D(uNoiseTex, vec2(offset_x, pos.y)).r - 0.5);

  // pos.x *= max(offsetData.x, 0.3) * 5.;
  float width = position.x;
  width *= 2.;
  // width *= 2. + cos(offset_x * 1000. + uTime * 4.);
  // width *= sin(position.y * 15. - 18. * uTime * (.55 - offsetData.x) + offsetData.x * 1007.3 ) * 4.;
  // width *= n * 2. + .5;
  // width *= 4.;
  vW = step(0.001, position.x);

  pos.x = offset_x + width * uThickness;

  // float pointerDist = abs(uPointerX - pos.x) * 30.;
  // float pointerSign = sign(pointerDist);
  // pointerDist = min(pointerDist, 1.5707963267);
  // float pointerFactor = cos(pointerDist);
  // pos.x += pointerSign * pointerFactor * 0.002 * offsetData.x;

  pos.xz = pos.xy;

  // float t = texture2D(uTransitionTex, pos.xz).r;
  // float _k = clamp(-1.0 + t + k * 2., 0.0, 1.0);

  // float _k = 0.;

  float uZeroZ = uZeroZ1;
  float uOneZ = uOneZ1;

  vec2 nuv = pos.xz;

  float noise = texture2D(uNoiseTex, nuv).r - 0.5;
  // float nx = cos(noise) * 0.1;
  // float ny = sin(noise) * 0.1;
  // float dz = pos.z - 0.5;
  // pos.x += (noise - 0.5) * dz * 2.;
  // noise = sin(pos.x * pos.y * 10. + uTime) * 0.5 + 0.5;
  float noiseSign = sign(noise);
  pos.x += pow(abs(noise), .95) * noiseSign * .3;

  // pos.y += ny;
  // pos.x += offsetData.z * 0.1;
  // pos.x += sin(offset_x * 3.) * 1.;
  // pos.x += (sin(offset_x * 10.0) * 0.5 + 0.5) * .2;
  // pos.x += sin(pos.z * 9.8 + uTime * 4.4) * cos(offset_x * 8. + uTime * 2.) * .1;


  vec4 vdata = texture2D(uVertTex1, pos.xy);
  vPosition = vdata.rgb;
  pos.z = 1.0 - pos.z;


  vOffsetColor.rgb = texture2D(uOffsetTex, vec2(offset_x, 0.25)).rgb;
  // vOffsetColor.rgb = vec3(0.)

  float o = smoothstep(uZeroZ, uOneZ, vPosition.z) * vdata.a;
  vec3 ndata = texture2D(uNormTex1, pos.xz).rgb;
  ndata *= 2.;
  ndata -= 1.;
  o *= dot(ndata, uLightDir) * uLightIntensity * 0.7;

  vOffsetColor.a = 1.;
  vOffsetColor.a *= clamp(o, 0., 1.);

  // float _noise = pow(abs(noise - 0.5), 0.3)
  // float q = cos(_noise * 3.5) + 0.5 + 0.5;
  // float e = cos(_noise * 3.5) + 0.5 + 0.5;
  // float s = sin(offset_x * 6. * offsetData.y - uTime * 2.) * 3.0 ;

  // float a =  cos(position.z * 8. + noise * 0. + offsetData.x * s + uTime * 1.) * 0.5 + 0.5;

  // float a = cos(position.y * 8. + offsetData.y * (0.5 + offsetData.x * 0.4)) * 0.25 + 0.8;
  float shiftDist = (position.y - offsetData.y) / uLineLength;
  float shiftSide = step(0.5, shiftDist);
  shiftDist = min(abs(shiftDist), 1.);

  // float q1 = pow(1. - shiftDist, 0.6);
  float q1 = cos(shiftDist * 3.1415) * 0.5 + 0.5;
  float q2 = pow(1. - shiftDist, .5);
  float a = mix(q2, q1, shiftSide);
  // shiftDist = min(shiftDist, 3.1415);
  // float a = cos(shiftDist) * 0.5 + 0.5;
  // float diversity = a;
  // float diversity = pow(offsetData.x, 4.) * pow(a * uMultiplier, 1.3);
  float diversity = pow(a, 0.3);
  // diversity = 1.;
  // diversity = a * (1. - offsetData.x * uDiversity);
  // diversity = pow(diversity, 1.5);
  vOffsetColor.a *= diversity;

  // vDiversity =  sin(offset_x * offsetData.x * 100.) * 0.5 + 0.5;
  // vOffsetColor.a = 1.;

  // vCol = vOffsetColor.rgb;
  // vOffsetColor.rgb -= vec3(0.02) * pow(vOffsetColor.a, 5.);
  // vOffsetColor.rgb += vec3(.13) * pow(vOffsetColor.a + .5, 4.) * uTwinkleMultiplier * 100.;

  // vPosition = mix(_pos, vPosition, o);
   // vPosition.z += 1. * (1. - noise) * 0.1;

   // vPosition.z += (1. - offsetData.x) * 0.001 * 15.;

   // vPosition.z += offsetData.x  * 0.02;
   vPosition.z += offsetData.x  * 0.015;
   // vPosition.z += vOffsetColor.a * 0.02 * uMovement;

   // vPosition.z += vOffsetColor.a * 0.01 * uMovement * (1. + dz * 2.);
   // vPosition.z += dz * 0.2;

  // vOffsetColor = noise;
  // vOffsetColor *= step(mod(offset_x, _uMod), _uStep);

  // vPosition = position;
  // vPosition.x = offset_x + width * uThickness;
  // vPosition.xy = pos.xz;

  gl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );
  // gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );
  vPosition = pos;
  // vK = _k;
  // gl_PointSize = 3.;
}