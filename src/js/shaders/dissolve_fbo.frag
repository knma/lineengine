precision highp float;

uniform sampler2D uEdgeTex1;
uniform sampler2D uEdgeTex2;
uniform sampler2D uDissolveTex;
uniform vec2 uEdgeResolution;

uniform float uDissolve;

varying vec2 vUv;

void main() {
  vec2 uv = vUv;
  vec2 pixelPos = floor(gl_FragCoord.xy);
  uv = pixelPos / uEdgeResolution;

  float edge1 = texture2D(uEdgeTex1, uv).a;
  float edge2 = texture2D(uEdgeTex2, uv).a;
  float dissolvePrev = texture2D(uDissolveTex, uv).r;

  gl_FragColor.r = dissolvePrev;

  bool changed = abs(edge1 - edge2) > 0.5;

  if (uDissolve > 0.5 && changed) {
    gl_FragColor.r = 1.;
  }

  if (uDissolve < 0.5 && changed) {
    gl_FragColor.r = 0.;
  }


  // float posX;
  // posX = pixelPos.y * uEdgeResolution.x + pixelPos.x;
  // posX /= uResolution.x;

  // vec4 lastPos = texture2D(uPositionTex, vec2(posX, 1.));

  // if (uDissolve > && lastPos.a > data.x) {
  //   gl_FragColor.r = 1;
  // } 

  // gl_FragColor = vec4(0.);
  // gl_FragColor.xy = uv;
}