uniform sampler2D uPrevTex;
uniform sampler2D uEdgeTex;
uniform float uSub;
varying vec2 vUv;

void main() {
  float prev = texture2D(uPrevTex, vUv).r;
  prev -= uSub;
  float edge = texture2D(uEdgeTex, vUv).r;
  gl_FragColor.r = mix(prev, edge, edge);
  gl_FragColor.a = 1.;
}




