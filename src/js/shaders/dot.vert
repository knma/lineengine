precision highp float;
uniform float uPSize;
void main(){
  gl_PointSize = uPSize;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}