uniform sampler2D uTex;
uniform sampler2D uMaskTex;
uniform sampler2D uNoiseTex;
uniform vec3 uLinesColor;
uniform float uOpacity;
varying vec2 vUv;

void main() {
  float mask = texture2D(uMaskTex, vUv).r;
  float val = texture2D(uTex, vUv).r;
  gl_FragColor.rgb = uLinesColor;
  gl_FragColor.a = val * mask;
  gl_FragColor.a *= uOpacity;
}




