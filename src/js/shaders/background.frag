precision highp float;
uniform sampler2D uGradientTex1;
uniform sampler2D uGradientTex2;
uniform float uTime;
uniform float uAngle;
uniform float k;
uniform float uScale;
uniform float uAngleNoise;
uniform float uXNoise;
uniform float uScaleNoise;
uniform float uDynamics;
uniform float uHue;
uniform float uSat;
uniform float uInverseTransition;
uniform float uValue;
uniform float uYShift;
uniform float uXShift;
uniform float uDissolve;
uniform float uOpacity;
// uniform float uSaturation;
// uniform float uValue;
uniform vec3 uColor;
// uniform float uMix;
// uniform vec3 uColor1;
// uniform vec3 uColor2;
// uniform float uAlpha1;
// uniform float uAlpha2;
varying vec2 vUv;

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
  vec2 uv = vUv;

  uv.y -= uYShift;

  uv -= 0.5;

  float angle = uAngle + (uAngleNoise) * uDynamics * 1.3;

  float dx = uXNoise * .5;
  uv.x += dx * uDynamics;

  uv.x = uv.x * cos(angle) - uv.y * sin(angle);
  uv.x -= uXShift ;

  float _uvx = mix(0.5 - uv.x, 0.5 + uv.x, step(0.5, uInverseTransition));
  float _k = smoothstep(0.0, 1.0, -1. + _uvx + k * 2.);
  float _kmixed = mix(_k, k, 0.75);

  uv.x *= uScale;
  uv.x += 0.5;
  uv.y = 0.0;
  vec4 bg = mix(texture2D(uGradientTex1, uv), texture2D(uGradientTex2, uv), _kmixed);

  vec3 hsv = rgb2hsv(bg.rgb);

  float dh = uHue + uXNoise * 0.03;
  float dv = uScaleNoise * 0.1;
  float ds = abs(uXShift) * .1;

  hsv.x += dh;
  hsv.y += ds;
  hsv.y *= uSat;
  hsv.z += dv + uValue;
  hsv.z *= (1. + uYShift * .6);

  bg.rgb = hsv2rgb(hsv);
  gl_FragColor = bg;
  gl_FragColor.rgb = mix(gl_FragColor.rgb, uColor, uDissolve);
  gl_FragColor.a = uOpacity;

}