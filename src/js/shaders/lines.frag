precision highp float;
uniform sampler2D uMaskTex1;
uniform sampler2D uNoiseTex;
uniform vec3 uLinesColor;
uniform float uSurrounding;
uniform float uOpacity;
uniform float uDissolve;
uniform float uAnimFactor;
uniform float uDy;
uniform float uHoverImpact;
varying vec3 vPosition;
varying vec4 vOffsetColor;
varying float vW;
varying float vDiversity;
void main() {

  vec2 uv = vPosition.xy;

  float mask = texture2D(uMaskTex1, uv).r;

  float  m = mask;
  float noise = texture2D(uNoiseTex, vec2(vPosition.x, 0.5 + (vPosition.y - 0.5) * .2)).r;

  float n = abs(noise - 0.5) + 0.1;
  float zlen = abs(vPosition.y - 0.5);
  float len = length(vPosition.xy - vec2(0.5)) * 2.;
  float cosArg = zlen * (17. - uAnimFactor * 2. - vOffsetColor.a * (pow(n, .8)) * 18.);
  cosArg = min(cosArg, 3.14);
  mask += min(cos(cosArg) * 0.5 + 0.5, 0.6) * n * vOffsetColor.a * (uSurrounding - uDissolve);

  gl_FragColor = vOffsetColor;

  gl_FragColor.rgb = uLinesColor;

  float a = pow(gl_FragColor.a * (.6 + vDiversity * 1.), 9. * vDiversity);
  float animA = pow(gl_FragColor.a * 1.4 * pow(vDiversity + 0.25, 3.), 15.);

  a = mix(a, animA, pow(uAnimFactor, 1.));

  if (m < 0.01) {
    mask *= (.9 - pow(len, 7.));
    gl_FragColor.a = a;
    mask *= pow(noise + 0.62, 4.);
  } else {
    gl_FragColor.a *= 1. + pow(vDiversity, 3.) * 2.5 ;
    gl_FragColor.a = mix(gl_FragColor.a, a, pow(uAnimFactor, .3));
  }
    
  mask = clamp(mask, 0., 1.);

  mask = mix(mask, 1. - zlen * 4., pow(uAnimFactor, 20.));

  mask *= smoothstep(vDiversity, vDiversity + 0.2, (1. - uDy) * 1.1);

  gl_FragColor.a = clamp(gl_FragColor.a * mask * (1. - vW * 0.), 0., 1.);

  gl_FragColor.a *= uOpacity;
}