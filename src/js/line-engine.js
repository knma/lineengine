"use strict";

require('./polyfills');
window.THREE = require('./lib/three.fixed');
require('./lib/OrbitControls');
require('./lib/PNGLoader');
var noise = require('./lib/perlin').noise;
var StateMachine = require('./lib/state-machine.min');
var easings = require('./easings');
var glslify = require('glslify');


var materialsShared = {
  'noise': new THREE.ShaderMaterial({
    uniforms: {
      uTime: { type: 'f' },
      uScale: { type: 'f' },
      uPointer: { type: "v2" },
    }, 
    vertexShader: glslify(__dirname + '/shaders/noise.vert'),
    fragmentShader: glslify(__dirname + '/shaders/noise.frag'),
    transparent: false,
  }),
  'noise_LE1': new THREE.ShaderMaterial({
    uniforms: {
      uTime: { type: 'f', value: 0 },
      uScale: { type: 'f', value: 1 },
    }, 
    vertexShader: glslify(__dirname + '/shaders/noise.vert'),
    fragmentShader: glslify(__dirname + '/shaders/noise_le1.frag'),
    transparent: false,
  }),
  basicWhite: new THREE.MeshBasicMaterial({
    color: 0xffffff,
    transparent: true,
    blending: THREE.NoBlending,
    depthWrite: false,
    depthTest: false,
  }),
  rv3Clone: new THREE.MeshBasicMaterial({
    color: 0xffffff,
    transparent: true,
    blending: THREE.AdditiveBlending,
    depthWrite: false,
    depthTest: false,
  }),
  rv3Grow: new THREE.ShaderMaterial( {
    uniforms: {
      uPrevTex: { type: "t" },
      uEdgeTex: { type: "t" },
      uSub: { type: "f" },
    },
    vertexShader: glslify(__dirname + '/shaders/rv3_grow.vert'),
    fragmentShader: glslify(__dirname + '/shaders/rv3_grow.frag'),
    transparent: false,
    blending:THREE.NoBlending,
    depthWrite: false,
  }),
  rv3Draw: new THREE.ShaderMaterial( {
    uniforms: {
      uTex: { type: "t" },
      uMaskTex: { type: "t" },
      uNoiseTex: { type: "t" },
      uOpacity: { type: "f" },
      uLinesColor: { type: "v3" },
    },
    vertexShader: glslify(__dirname + '/shaders/rv3_draw.vert'),
    fragmentShader: glslify(__dirname + '/shaders/rv3_draw.frag'),
    transparent: true,
    depthWrite: false,
  }),
  rv3Dots: new THREE.ShaderMaterial( {
    uniforms: {
      uTex: { type: "t" },
      uPSize: { type: "f", value: 0 },
    },
    vertexShader: glslify(__dirname + '/shaders/dot.vert'),
    fragmentShader: glslify(__dirname + '/shaders/dot.frag'),
    transparent: true,
    depthWrite: false,
  }),
  linesDraw: new THREE.RawShaderMaterial( {
    uniforms: {
      uPosTex1: { type: "t" },
      uOffsetTex: { type: "t" },
      uNoiseTex: { type: "t" },
      uMaskTex1: { type: "t" },
      uTime: { type: "f" },
      uDissolve: { type: "f" },
      uDy: { type: "f" },
      uHoverImpact: { type: "f" },
      uPointer: { type: "v2" },
      uLinesColor: { type: "v3" },
      uPointerXScreen: { type: "f" },
      uPointerPower: { type: "f" },
      uAnimFactor: { type: "f" },
      uMask: { type: "f" },
      uThickness: { type: "f" },
      uOpacity: { type: "f" },
      uSurrounding: { type: "f" },
      uCameraPos: { type: "v3" },
    },
    vertexShader: glslify(__dirname + '/shaders/lines.vert'),
    fragmentShader: glslify(__dirname + '/shaders/lines.frag'),
    transparent: true,
    side: THREE.DoubleSide,
    depthWrite: false,
  }),
  linesDraw_LE1: new THREE.RawShaderMaterial( {
    uniforms: {
      uVertTex1: { type: "t" },
      uVertTex2: { type: "t" },
      uNormTex1: { type: "t" },
      uNormTex2: { type: "t" },
      uOffsetTex: { type: "t" },
      uNoiseTex: { type: "t" },
      uMaskTex1: { type: "t" },
      uMaskTex2: { type: "t" },
      uOpacity: { type: "f" },
      uZeroZ1: { type: "f" },
      uZeroZ2: { type: "f" },
      uOneZ1: { type: "f" },
      uOneZ2: { type: "f" },
      uTime: { type: "f" },
      uLineLength: { type: "f" },
      uDiversity: { type: "f" },
      uPointerX: { type: "f" },
      k: { type: "f" },
      uThickness: { type: "f" },
      uMovement: { type: "f" },
      uMultiplier: { type: "f" },
      uSurrounding: { type: "f" },
      uLightDir: { type: "v3" },
      uLightIntensity: { type: "f" },
      uTwinkleMultiplier: { type: "f" },
    },
    vertexShader: glslify(__dirname + '/shaders/lines_le1.vert'),
    fragmentShader: glslify(__dirname + '/shaders/lines_le1.frag'),
    transparent: true,
    blendSrc: THREE.OneFactor,
    side: THREE.BackSide,
    blendDst: THREE.OneMinusSrcAlphaFactor,
    blendEquation: THREE.AddEquation
  }),
  inqDraw: new THREE.ShaderMaterial( {
    uniforms: {
      uPosTex1: { type: "t" },
      uOffsetTex: { type: "t" },
      uNoiseTex: { type: "t" },
      uTime: { type: "f" },
      uLinesColor: { type: "v3" },
      uThickness: { type: "f" },
      uOpacity: { type: "f" },
      uFlick: { type: "f" },
      uCameraPos: { type: "v3" },
    },
    vertexShader: glslify(__dirname + '/shaders/inq.vert'),
    fragmentShader: glslify(__dirname + '/shaders/inq.frag'),
    transparent: true,
    side: THREE.DoubleSide,
    depthWrite: false,
  }),
  background: new THREE.ShaderMaterial( {
    uniforms: {
      uDissolve: { type: "f" },
      k: { type: "f" },
      uColor: { type: "v3" },
      uMix: { type: "f" },
      uGradientTex1: { type: "t" },
      uGradientTex2: { type: "t" },
      uTime: { type: "f" },
      uValue: { type: "f" },
      uYShift: { type: "f" },
      uXShift: { type: "f" },
      uAngleNoise: { type: "f" },
      uInverseTransition: { type: "f" },
      uXNoise: { type: "f" },
      uScaleNoise: { type: "f" },
      uDynamics: { type: "f" },
      uHue: { type: "f" },
      uSat: { type: "f" },
      uAngle: { type: "f" },
      uScale: { type: "f" },
      uOpacity: { type: "f" },
    },
    vertexShader: glslify(__dirname + '/shaders/background.vert'),
    fragmentShader: glslify(__dirname + '/shaders/background.frag'),
    transparent: true,
  }),
  positionFbo: new THREE.ShaderMaterial( {
    uniforms: {
      uEdgeTex: { type: "t" },
      uEdgeDataTex: { type: "t" },
      uEdgeInitialTex: { type: "t" },
      uPositionTex: { type: "t" },
      uResolution: { type: "v2" },
      uEdgeFboSide: { type: "f" },
      uPointer: { type: "v2" },
      uvStepAdd: { type: "f" },
    },
    vertexShader: glslify(__dirname + '/shaders/positions_fbo.vert'),
    fragmentShader: glslify(__dirname + '/shaders/positions_fbo.frag'),
    transparent: true,
    blending: THREE.NoBlending,
    depthWrite: false,
    depthTest: false,
  }),
  edgeFbo: new THREE.ShaderMaterial( {
    uniforms: {
      uEdgeTex: { type: "t" },
      uEdgeDataTex: { type: "t" },
      uEdgeInitialTex: { type: "t" },
      uPositionTex: { type: "t" },
      uVertTex1: { type: "t" },
      uNoiseTex: { type: "t" },
      uPointerFbo: { type: "t" },
      uPointer: { type: "v2" },
      uResolution: { type: "v2" },
      uEdgeResolution: { type: "v2" },
      uTime: { type: "f" },
      uZNoise: { type: "f" },
      uNoiseAmount: { type: "f" },
    },
    vertexShader: glslify(__dirname + '/shaders/edge_fbo.vert'),
    fragmentShader: glslify(__dirname + '/shaders/edge_fbo.frag'),
    transparent: true,
    blending: THREE.NoBlending,
    depthWrite: false,
    depthTest: false,
  }),
};

var texturesShared = {
  dot: { fileName: 'dot.jpg' },
  mask: { dataURL: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAFElEQVQYV2P8////fwY8gHFkKAAANHof6WnTqJEAAAAASUVORK5CYII=' },
  black: { dataURL: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAE0lEQVQIW2NkYGD4z4AEGEkXAACaVAQB3CT+PAAAAABJRU5ErkJggg==' },
};

var states = {};
var transitions = {};
var positions = {};
var assetsPath;
var settled = false;
var sharedDataLoaded = false;
var glFeatures = {};
var renderVersion = 4;
var floatType = THREE.UnsignedByteType;
var MAX_HALF_FLOAT = 65535.0;
var isTouchDevice = 'ontouchstart' in document.documentElement;
var userAgent = navigator.userAgent || navigator.vendor || window.opera;
var isIos = /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream;
var webgl = (function () {
  try {
    var canvas = document.createElement( 'canvas' );
    var gl = window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) );
    glFeatures.singleFloat = gl.getExtension('OES_texture_float');
    glFeatures.singleFloatLinear = gl.getExtension('OES_texture_float_linear');
    glFeatures.instancing = gl.getExtension('ANGLE_instanced_arrays');
    glFeatures.vertexUnits = gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    if (!!glFeatures.singleFloat && !!glFeatures.singleFloatLinear && !!glFeatures.instancing && (glFeatures.vertexUnits >= 4)) {
      (function() {
        renderVersion = 2;
        var texture = gl.createTexture();
        gl.deleteTexture(texture);
        var texture = gl.createTexture();
        floatType = THREE.FloatType;
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 2, 2, 0, gl.RGBA, gl.FLOAT, null);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        var framebuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D,  texture, 0);
        glFeatures.singleFloatFbo = gl.checkFramebufferStatus(gl.FRAMEBUFFER) === gl.FRAMEBUFFER_COMPLETE;
        if (glFeatures.singleFloatFbo) {
          renderVersion = 1;
        }
        gl.deleteTexture(texture);
        gl.deleteFramebuffer(framebuffer);
        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      })();
    } else {
      renderVersion = 3;
    }
    return true;
  } catch (e) {
    return false;
  }
})();

function setup(params) {
  params = params || {};
  if (typeof params.states !== 'object' || Array.isArray(params.states) || !Object.keys(params.states).length) {
    console.error('LineEngine: setup: no states specified');
    return;
  }
  if (typeof params.assetsPath !== 'string') {
    console.error('LineEngine: setup: assetsPath is not specified');
    return;
  }
  if (settled) {
    console.error('LineEngine: already settled');
    return;
  }
  // for (var i = 0; i < params.transitions.length; i++) {
  //   var tName = params.transitions[i];
  //   if (typeof tName === 'string') {
  //     transitions[tName] = {};
  //   }
  // }
  // if (!Object.keys(params.transitions).length) {
  //   console.error('LineEngine: setup: no transitions specified');
  //   return;
  // }
  if (params.forceRenderVersion && params.forceRenderVersion <= 4 && params.forceRenderVersion > renderVersion) {
    renderVersion = params.forceRenderVersion;
  }
  assetsPath = params.assetsPath;
  for (var stateName in params.states) {
    if (params.states.hasOwnProperty(stateName)) {
      var state = params.states[stateName];
      var _state = states[stateName] = {};
      var model = state.type === 'model';
      var subPath = state.subPath || '';
      _state.textures = {};
      _state.type = state.type;
      var smallNum = 0.00000001;
      if (state.type === 'displacementMap' || state.type === 'model') {
        _state.lines = {
          url: params.assetsPath + subPath + stateName + '.png'
        };
        _state.normals = {
          url: params.assetsPath + subPath + stateName + '_normals.png'
        };
        _state.model = {
          url: params.assetsPath + subPath + stateName + '.obj'
        };
        _state.scale = parseFloat(state.scale) || 1.0
        if (!model) {
          _state.model = null;
          _state.displacementAmount = state.displacementAmount || 0;
        }
        if (model && state.lightIntensity) {
          _state.lightIntensity = state.lightIntensity;
        } else {
          _state.lightIntensity = 1;
        }
        if (model && state.lightDirection) {
          _state.lightDirection = new THREE.Vector3(state.lightDirection.x, state.lightDirection.y, state.lightDirection.z);
        } else {
          _state.lightDirection = null;
        }
        if (state.textColor !== 0x000000 && state.textColor) {
          _state.textColor = new THREE.Color(state.textColor);
          _state.useTextColor = true;
        } else {
          _state.textColor = new THREE.Color(0x000000);
          _state.useTextColor = false;
        }
        if (typeof state.bgFadeColor !== 'undefined') {
          _state.bgFadeColor = new THREE.Color(state.bgFadeColor);
        } else {
          _state.bgFadeColor = new THREE.Color(0x000000);
        }
        _state.zZero = state.zZero || 0.3;
        _state.zOne = state.zOne || 0.5;
        _state.mask = typeof state.mask !== 'undefined' ? state.mask : false;
        _state.viewR = state.viewR || 1;
        _state.viewH = state.viewH || smallNum;
        _state.viewV = state.viewV || Math.PI / 2;
        _state.colorMultiplier = state.colorMultiplier || 1;
        _state.pointSize = state.pointSize || 1;
        _state.movement = typeof state.movement === 'number' ? state.movement : 0;
        _state.twinkleMultiplier = typeof state.twinkleMultiplier === 'number' ? state.twinkleMultiplier : 0;
        _state.noiseAmount = typeof state.noiseAmount === 'number' ? state.noiseAmount : 0;
        _state.opacityAddition = typeof state.opacityAddition === 'number' ? state.opacityAddition : 0;
        _state.seed = state.seed;
        _state.frame = state.frame;
        _state.renderVersion = state.renderVersion || null;
        _state.fallbackState = state.fallbackState;
        _state.colorAdvantage = typeof state.colorAdvantage !== 'undefined' ? state.colorAdvantage : 0;
        _state.lineLength = typeof state.lineLength !== 'undefined' ? state.lineLength : 0.3;
        _state.diversity = typeof state.diversity !== 'undefined' ? state.diversity : 0;
      }
      if (state.type === 'background') {
        _state.viewR = null;
        _state.viewH = null;
        _state.viewV = null;
        _state.parentState = state.parentState;
      }
      if (state.type === 'inquiry') {
        _state.renderVersion = state.renderVersion || null;
        _state.viewR = null;
        _state.viewH = null;
        _state.viewV = null;
      }
      if (typeof state.linesColor !== 'undefined') {
        _state.linesColor = new THREE.Color(state.linesColor);
      } else {
        _state.linesColor = new THREE.Color(0xffffff);
      }
      _state.pointerImpact = typeof state.pointerImpact !== 'undefined' ? state.pointerImpact : smallNum;
      _state.subPath = subPath;
      _state.surrounding = typeof state.surrounding === 'number' ? state.surrounding : 0;
      _state.linesGradient = typeof state.linesGradient !== 'undefined' ? state.linesGradient : false;
      _state.backgroundGradient = typeof state.backgroundGradient !== 'undefined' ? state.backgroundGradient : false;
      _state.backgroundVariations = typeof state.backgroundVariations !== 'undefined' ? state.backgroundVariations : null;
      _state.backgroundAngle = typeof state.backgroundAngle !== 'undefined' ? state.backgroundAngle : Math.PI / 2;
      _state.backgroundScale = state.backgroundScale || 1.0;
      _state.backgroundDynamics = state.backgroundDynamics || 0;
      _state.bgTransitionInverted = !!state.bgTransitionInverted;
      _state.dHues = Array.isArray(state.dHues) && state.dHues.length ? state.dHues : [0];
      _state.dHue = _state.dHues[0];
      _state.thickness = typeof state.thickness === 'number' ? state.thickness : 1;
      _state.fallbackImage = typeof state.fallbackImage !== 'undefined' ? state.fallbackImage : false;
    }
  }
  settled = true;
}


function loadStates(_stateNames, onProgress) {
  return new Promise(function(resolve, reject) {
    if (_stateNames === 'all') {
      _stateNames = Object.keys(states);
    }
    if (!Array.isArray(_stateNames) || !_stateNames.length) {
      reject('no states specified');
      return;
    }
    if (!settled) {
      reject('LineEngine is not settled');
      return;
    }
    var loadingManager = new THREE.LoadingManager();
    loadingManager.onLoad = function() {
      for (var i = 0; i < _stateNames.length; i++) {
        var stateName = _stateNames[i];
        if (states.hasOwnProperty(stateName)) {
          states[stateName].loaded = true;
        }
      }
      sharedDataLoaded = true;
      resolve(states);
    };
    typeof onProgress === 'function' && (loadingManager.onProgress = onProgress);
    loadingManager.onError = function(what) {
      reject('cannot load ' + what);
    }
    var pngLoader = new THREE.XHRPNGLoader(loadingManager);
    var textureLoader = new THREE.TextureLoader(loadingManager);
    var ImageLoader = new THREE.ImageLoader(loadingManager);
    for (var i = 0; i < _stateNames.length; i++) {
      var stateName = _stateNames[i];
      if (!states.hasOwnProperty(stateName)) continue;
      var state = states[stateName];
      if (typeof state.mask === 'string') {
        if (_stateNames.indexOf(state.mask) < 0) {
          console.warn('LineEngine: wrong mask \'' + state.mask);
        } else {
          states[state.mask].maskExpectants = states[state.mask].maskExpectants || [];
          states[state.mask].maskExpectants.push(stateName);
        }
      }
      if (typeof state.linesGradient === 'string') {
        if (_stateNames.indexOf(state.linesGradient) < 0) {
          console.warn('LineEngine: wrong lines gradient \'' + state.linesGradient);
        } else {
          states[state.linesGradient].linesGradientExpectants = states[state.linesGradient].linesGradientExpectants || [];
          states[state.linesGradient].linesGradientExpectants.push(stateName);
        }
      }
      if (typeof state.backgroundGradient === 'string') {
        if (_stateNames.indexOf(state.backgroundGradient) < 0) {
          console.warn('LineEngine: wrong background gradient \'' + state.backgroundGradient);
        } else {
          states[state.backgroundGradient].backgroundGradientExpectants = states[state.backgroundGradient].backgroundGradientExpectants || [];
          states[state.backgroundGradient].backgroundGradientExpectants.push(stateName);
        }
      }
    }
    for (var i = 0; i < _stateNames.length; i++) {
      var stateName = _stateNames[i];
      if (!states.hasOwnProperty(stateName)) {
        console.warn('LineEngine: state \'' + stateName + '\' doesn\'t exist');
        continue;
      }
      var state = states[stateName];
      if (state.renderVersion === 1 && renderVersion > 1) {
        if (typeof state.fallbackState === 'string' && states.hasOwnProperty(state.fallbackState)) {
          states[stateName] = states[state.fallbackState];
          continue;
        } else {
          reject('Fallback state is not specified');
          return;
        }
      };
      if (state.fallbackImage && (state.type === 'inquiry' || state.renderVersion === 3 && renderVersion > 3 || state.renderVersion === 2 && (renderVersion > 2 || isIos))) {
        (function(stateName) {
          var url = assetsPath + state.subPath + stateName + '_fallback.jpg';
          var urlText = assetsPath + state.subPath + stateName + '_fallback_text.png';
          ImageLoader.load(url, function(image) {
            var state = states[stateName];
            state.fbImage = image;
            state.fbImageUrl = url;
          });
          if (state.renderVersion === 3 && state.type !== 'inquiry') {
            ImageLoader.load(urlText, function(image) {
              var state = states[stateName];
              state.fbImageText = image;
              state.fbImageUrlText = urlText;
            });
          }
        })(stateName);
      }
      if (renderVersion > 3) {
        continue;
      }
      if (state.loaded) {
        console.warn('LineEngine: state \'' + stateName + '\' is already loaded');
        continue;
      }
      if (state.model || state.displacementAmount) {
        (function(lines) {
          pngLoader.load(lines.url, function (rawImage) {
            lines.rawImage = rawImage;
          });
        })(state.lines);
      }
      if (state.model) {
        (function(normals) {
          pngLoader.load(normals.url, function (rawImage) {
            normals.rawImage = rawImage;
          });
        })(state.normals);
      }
      if (state.mask && typeof state.mask !== 'string') {
        (function(stateName) {
          var url = assetsPath + state.subPath + stateName + '_mask.jpg';
          textureLoader.load(url, function(texture) {
            var state = states[stateName];
            state.textures['mask'] = texture;
            if (Array.isArray(state.maskExpectants)) {
              for (var i = 0; i < state.maskExpectants.length; i++) {
                states[state.maskExpectants[i]].textures['mask'] = texture;
              }
            }
          });
        })(stateName);
      }
      if (state.linesGradient && typeof state.linesGradient !== 'string') {
        (function(stateName) {
          var url = assetsPath + state.subPath + stateName + '_lines_gradient.jpg';
          textureLoader.load(url, function(texture) {
            var state = states[stateName];
            states[stateName].textures['lines_gradient'] = texture;
            if (Array.isArray(state.linesGradientExpectants)) {
              for (var i = 0; i < state.linesGradientExpectants.length; i++) {
                states[state.linesGradientExpectants[i]].textures['lines_gradient'] = texture;
              }
            }
          });
        })(stateName);
      }
      if (state.backgroundGradient && typeof state.backgroundGradient !== 'string') {
        (function(stateName) {
          var state = states[stateName];
          var times = state.backgroundVariations || 1;
          for (var g = 0; g < times; g++) {
            (function(g) {
              var urlAdd = '';
              if (state.backgroundVariations) {
                urlAdd = '_' + g;
              }
              var url = assetsPath + state.subPath + stateName + '_bg_gradient' + urlAdd + '.png';
              // var bgname = 'bg_gradient' + urlAdd;
              // textureLoader.load(url, function(tex) {
              //   state.textures[bgname] = tex;
              // });
              // return;
              pngLoader.load(url, function (rawImage) {
                var channelsNum = Math.round(rawImage.buffer.length / rawImage.width / rawImage.height);
                var stride = rawImage.width * channelsNum;
                var buf = new Float32Array(stride * rawImage.height);
                var buffer = rawImage.buffer;
                for (var y = 0; y < rawImage.height; y++) {
                  for (var x = 0; x < rawImage.width; x++) {
                    var index = y * stride + x * channelsNum;
                    buf[index] = buffer[index] / MAX_HALF_FLOAT;
                    buf[index + 1] = buffer[index + 1] / MAX_HALF_FLOAT;
                    buf[index + 2] = buffer[index + 2] / MAX_HALF_FLOAT;
                    if (channelsNum === 4) {
                      buf[index + 3] = buffer[index + 3] / MAX_HALF_FLOAT;
                    }
                  }
                }
                var bgname = 'bg_gradient' + urlAdd;
                state.textures[bgname] = new THREE.DataTexture(
                  buf,
                  rawImage.width,
                  rawImage.height,
                  channelsNum === 3 ? THREE.RGBFormat : THREE.RGBAFormat,
                  floatType, // TODO
                  null,
                  // THREE.RepeatWrapping,
                  // THREE.RepeatWrapping,
                  THREE.ClampToEdgeWrapping,
                  THREE.ClampToEdgeWrapping,
                  THREE.LinearFilter,
                  THREE.LinearFilter
                );
                state.textures[bgname].needsUpdate = true;
                if (Array.isArray(state.backgroundGradientExpectants)) {
                  for (var i = 0; i < state.backgroundGradientExpectants.length; i++) {
                    states[state.backgroundGradientExpectants[i]].textures['bg_gradient'] = state.textures['bg_gradient'];
                  }
                }
              });
            })(g);
          }
        })(stateName);
      }
    }
    if (!sharedDataLoaded) {
      for (var texName in texturesShared) {
        if (texturesShared.hasOwnProperty(texName)) {
          (function(texName) {
            var texShared = texturesShared[texName];
            if (texShared.fileName) {
              var url = assetsPath + texShared.fileName;
            } else {
              var url = texShared.dataURL;
            }
            textureLoader.load(url, function(texture) {
              texShared.texture = texture;
            });
          })(texName);
        }
      }
      // if (renderVersion < 4) {
      //   for (var transitionName in transitions) {
      //     if (transitions.hasOwnProperty(transitionName)) {
      //       (function(transitionName) {
      //         var transition = transitions[transitionName];
      //         var url = assetsPath + 'transitions/' + transitionName + '.jpg';
      //         textureLoader.load(url , function(texture) {
      //           transition.texture = texture;
      //         });
      //       })(transitionName);
      //     }
      //   }
      // }
    }
  });
}


function createLines(options) {
  var fsm;
  var callbacks;
  var rafInstance;
  var renderer;
  var camera;
  var cameraOrtho;
  var composeScene;
  var linesScene;
  var planeMesh;
  var intersectPlaneMesh;
  var bgPlaneMesh;
  var linesMesh;
  var linesParent;
  var materials = {};
  var textures = {};
  var domEvents = {};
  var offsetDataTexture;
  var offsetDataShift;
  var offsetDataSpeed;
  var offsetArray;
  var currentState;
  var nextState;
  var _states;
  var raycaster;

  var startPositionsTexture;
  var initialPositionsTexture;
  var positionDataTexture;
  var initialPosArray;
  var posDataArray;

  var noiseFbo;
  var noiseFbo_LE2;
  var positionFbo;
  var edgeFbo;
  var bgFbo;

  var rv3LinesFbo;
  var rv3LinesFbos = [];
  var rv3Plane;
  var rv3Scene;
  var rv3Edge;
  var rv3Geometry;

  var pointers = new Array(30);

  var config = {
    dataFboSide: 512,
    positionFboSide: 512,
    lineDotsNumber: 100,
    modelLineDotsNumber: 200,
    lineInstancesNumber: 512,
    edgeFboSide: 32,
    rv3FboSide: 2048,
    bgFboSide: 256,
    rv3EdgeSide: 512,
    isReactiveToPointerInput: true,
    strokeColor: 0xffffff,
    extensions: {},
    noiseSize: 128,
    transitionDuration: 3000,
    viewV: Math.PI / 2,
    viewH: 0,
    viewR: 1.5,
    inertialK: 0.1,
    defaultEasingName: 'linear',
    fov: 50,
    lineLength: 0.3,
  };

  var PASS = {COMPOSED: {}, LIGHT: {}, LINES: {}, NOISE:{}, DOTS: {}}; // DEBUG
  var now = {
    x: window.innerWidth / 2,
    y: window.innerHeight / 2,
    frame: 0,
    noiseFrame: 0,
    active: true,
    transitionK: null,
    _pass: PASS.COMPOSED,
    viewV: config.viewV,
    viewH: config.viewH,
    viewR: config.viewR,
    viewVTarget: config.viewV,
    viewHTarget: config.viewH,
    viewRTarget: config.viewR,
    viewDH: 0,
    viewDV: 0,
    depthMaskDZ: 0,
    depthMaskDZTarget: 0,
    seed: Math.random() * 1000,
    startTime: new Date().getTime(),
    opacity: 1,
    fboSpeed: 2,
    hue: 0,
    hueTarget: 0,
    dissolve: 0,
    bgColor: new THREE.Color(0x000000),
    dissolveScale: 1,
    bgDissolve: 0,
    dy: 0,
    opacityMul: 1,
    bgOpacity: 1,
    dyWorld: 0,
    dxScreenTarget: 0,
    dyScreenTarget: 0,
    dxScreen: 0,
    dyScreen: 0,
    bgSat: 1,
    bgFlick: 0
  };

  var lines = {
    init: function() {
      Object.assign(config, options);

      lines.logo = options.logo;
      lines.canvas = options.canvas;

      if (!Array.isArray(options.states) || !Object.keys(options.states).length) {
        console.error('LineEngine: setup: no states specified');
        return;
      }

      if (!config.canvas) {
        console.error('LineEngine: setup: no canvas specified');
        return;
      }

      //  State machine 
      var events = [];
      callbacks = {
        onbeforeevent: function(event, from, to) {},
        onleavestate: function(event, from, to) {},
        onenterstate: function(event, from, to) {},
        onafterevent: function(event, from, to) {},
        ontransition: function(from, to, progress) {},
        ondraw: function(state) {},
      };
      var stateNames = config.stateNames = config.states;
      var allStateNames = ['none'];
      for (var i = 0; i < stateNames.length; i++) {
        var stateName = stateNames[i];
        if (!states.hasOwnProperty(stateName)) {
          console.warn('LineEngine: state \'' + stateName + '\' doesn\'t exist');
          continue;
        }
        allStateNames.push(stateName);
      }
      if (allStateNames.indexOf(options.initialState) < 0) {
        console.error('LineEngine: setup: wrong initialState');
        return;
      }

      var requiredRV = null;
      _states = {};
      for (var i = 1; i < allStateNames.length; i++) {
        var sname = allStateNames[i];
        var _state = states[sname];
        _states[sname] = states[sname];
        _states[sname].name = sname;
        if (_state.renderVersion) {
          if (requiredRV && requiredRV !== _state.renderVersion) {
            console.error('LineEngine: create: render versions within instance must be equal');
            return;
          } else {
            requiredRV = _state.renderVersion;
          }
        }
      }
      if (!requiredRV) {
        console.error('LineEngine: create: no state with specified render version exists');
        return;
      }
      if (renderVersion <= requiredRV) {
        now.renderVersion = requiredRV;
      } else {
        now.renderVersion = 4;
      }
      var isModel = false;
      for (var st in _states) {
        if (!_states.hasOwnProperty(st)) continue;
        if (_states[st].type === 'model') {
          isModel = true;
        }
        if (_states[st].type === 'inquiry') {
          now.isInq = true;
        }
      }
      if (isModel && isIos) {
        now.renderVersion = 4;
      }

      console.log('LineEngine: render version: ' + now.renderVersion + ' settled');

      options.callbacks = options.callbacks || {};
      for (var cbName in options.callbacks) {
        if (cbName === 'onleavestate') continue;
        var cbNameLC = cbName.toLowerCase();
        if (Object.keys(callbacks).indexOf(cbNameLC) >= 0) {
          (function(cbNameLC, cbName) {
            var cb = callbacks[cbNameLC];
            callbacks[cbNameLC] = function(param1, param2, param3) {
              cb(param1, param2, param3);
              options.callbacks[cbName](lines, param1, param2, param3);
            }
          })(cbNameLC, cbName);
        } else {
          (function(cbNameLC, cbName) {
            callbacks[cbNameLC] = function(param1, param2, param3) {
              options.callbacks[cbName](lines, param1, param2, param3);
            }
          })(cbNameLC, cbName);
        }
      }
      var onleavecb = callbacks['onleavestate'];
      if (options.callbacks.hasOwnProperty('onleavestate')) {
        callbacks['onleavestate'] = function(event, from, to) {
          if (from !== 'none') {
            onleavecb(event, from, to);
            options.callbacks['onleavestate'](lines, event, from, to);
            return StateMachine.ASYNC;
          }
        }
      } else {
        callbacks['onleavestate'] = function(event, from, to) {
          if (from !== 'none') {
            onleavecb(event, from, to);
            return StateMachine.ASYNC;
          }
        }
      }
      for (var i = 0; i < stateNames.length; i++) {
        var stateName = stateNames[i];
        var event = {
          name: 'to' + stateName,
          from: allStateNames,
          to: stateName
        };
        events.push(event);
      }
      fsm = StateMachine.create({
        initial: options.initialState,
        events: events,
        callbacks: callbacks
      });

      // now.transition = Object.keys(transitions)[0];
      currentState = _states[config.initialState];
      nextState = currentState;

      if (now.renderVersion === 4) {
        lines.resize();
        if (currentState.fbImage) {
          lines.canvas.style.backgroundImage = 'url(' + currentState.fbImageUrl + ')';
          now.fbImageUrl = currentState.fbImageUrl;
        }
        lines.animate();
        lines.pause();

        now.text = false;
        for (var st in _states) {
          if (_states.hasOwnProperty(st) && _states[st].fbImageUrlText) {
            now.fbImageText = _states[st].fbImageText;
            now.text = true;
          }
        }
        if (now.text) {
          now.fbImageText.style.position = 'absolute';
          now.fbImageText.style.top = 0 + 'px';
          now.fbImageText.style.left = 0 + 'px';
          var zi = lines.canvas.style.zIndex;
          if (zi) {
            now.fbImageText.style.zIndex = zi + 1;
          } 
          lines.canvas.parentNode.appendChild(now.fbImageText)
        }
        lines.resize();
        if (typeof options.onReady === 'function') {
          options.onReady(lines);
        }
        return;
      }

      // Renderer
      renderer = new THREE.WebGLRenderer({
        alpha: true,
        canvas: config.canvas,
        autoClear: now.renderVersion !== 3,
        antialias: true,
        premultipliedAlpha: false,
        // preserveDrawingBuffer: true
      });
      renderer.autoClear = now.renderVersion !== 3;
      renderer.getContext().enable(renderer.getContext().DITHER);
      renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1);

      textures = texturesShared;
      for (var matName in materialsShared) {
        if (materialsShared.hasOwnProperty(matName)) {
          materials[matName] = materialsShared[matName].clone();
        }
      }

      // Common items
      cameraOrtho = new THREE.OrthographicCamera(0, 1, 1, 0, -10, 10);
      cameraOrtho.position.z = 2;
      camera = new THREE.PerspectiveCamera(config.fov, config.canvas.width / config.canvas.height, 0.001, 100);
      camera.position.set(0.45, 0.45, 1.85);
      camera.lookAt(new THREE.Vector3(0.5, 0.5, 0.5));
      composeScene = new THREE.Scene();
      linesScene = new THREE.Scene();
      var planeGeometry = new THREE.PlaneBufferGeometry(1, 1);
      planeMesh = new THREE.Mesh(planeGeometry, materials['compose']);
      planeMesh.position.set(0.5, 0.5, 0.5);
      composeScene.add(planeMesh);
      intersectPlaneMesh = new THREE.Mesh(planeGeometry, new THREE.MeshBasicMaterial({color: 0x333333}));
      intersectPlaneMesh.material.visible = false;
      intersectPlaneMesh.position.set(0.5, 0.5, 0.5);
      raycaster = new THREE.Raycaster();
      linesScene.add(intersectPlaneMesh);
      linesScene.add(camera);

      // Background
      bgPlaneMesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), materials['basicWhite'].clone());
      bgPlaneMesh.position.z = -2.;
      bgPlaneMesh.renderOrder = 0;
      bgPlaneMesh.frustumCulled = false;
      camera.add(bgPlaneMesh);

      if (now.renderVersion === 1 && !now.isInq) {
        noiseFbo_LE2 = new THREE.WebGLRenderTarget(config.noiseSize, config.noiseSize, {
          minFilter: THREE.LinearFilter,
          magFilter: THREE.LinearFilter,
          format: THREE.RGBAFormat,
          wrapS: THREE.ClampToEdgeWrapping,
          wrapT: THREE.ClampToEdgeWrapping,
          type: glFeatures.singleFloatFbo ? THREE.FloatType : (glFeatures.halfFloatFbo ? THREE.HalfFloatType : THREE.UnsignedByteType),
          depthBuffer: false,
          stencilBuffer: false
        });

        var edgeFboSide = config.edgeFboSide;
        var posDataArraySize = edgeFboSide * edgeFboSide * 4;
        initialPosArray =  new Float32Array( posDataArraySize ); 
        posDataArray = new Float32Array( posDataArraySize ); 
        for (var y = 0; y < edgeFboSide; y++) {
          for (var x = 0; x < edgeFboSide; x++) {
            var index = y * edgeFboSide * 4 + x * 4;
            initialPosArray[index] = (y * edgeFboSide + x) / config.positionFboSide;
            initialPosArray[index + 1] = 0.2;
            initialPosArray[index + 2] = 0.5;
            initialPosArray[index + 3] = - Math.random() * 0.8;
            posDataArray[index] = 0.9; // death distance
            posDataArray[index + 1] = 1 / config.positionFboSide * 2.3; // grow step
            posDataArray[index + 2] = 0.8 + Math.random() * 0.2;
          }
        }
        initialPositionsTexture = new THREE.DataTexture(
          initialPosArray,
          edgeFboSide,
          edgeFboSide,
          THREE.RGBAFormat,
          floatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        positionDataTexture = new THREE.DataTexture(
          posDataArray,
          edgeFboSide,
          edgeFboSide,
          THREE.RGBAFormat,
          floatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        initialPositionsTexture.needsUpdate = true;
        positionDataTexture.needsUpdate = true;

        // Start Positions
        var posArraySize = edgeFboSide * edgeFboSide * 4;
        var initialArray =  new Float32Array( posArraySize ); 
        for (var y = 0; y < edgeFboSide; y++) {
          for (var x = 0; x < edgeFboSide; x++) {
            var ind = y * edgeFboSide * 4 + x * 4;
            initialArray[ind] = (y * edgeFboSide + x) / config.positionFboSide;
            initialArray[ind + 1] = 0.2;
            initialArray[ind + 2] = 0.;
            initialArray[ind + 3] = 0.75;
          }
        }
        startPositionsTexture = new THREE.DataTexture(
          initialArray,
          edgeFboSide,
          edgeFboSide,
          THREE.RGBAFormat,
          floatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        startPositionsTexture.needsUpdate = true;

        positionFbo = [];
        edgeFbo = [];
        now.currentPosFboIndex = 0;
        for (var i = 0; i < 2; i++) {
          positionFbo.push(new THREE.WebGLRenderTarget(config.positionFboSide, config.positionFboSide, {
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            wrapS: THREE.ClampToEdgeWrapping,
            wrapT: THREE.ClampToEdgeWrapping,
            type: floatType,
            depthBuffer: false,
            stencilBuffer: false,
            premultipliedAlpha: false,
          }));
          edgeFbo.push(new THREE.WebGLRenderTarget(edgeFboSide, edgeFboSide, {
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            wrapS: THREE.ClampToEdgeWrapping,
            wrapT: THREE.ClampToEdgeWrapping,
            type: floatType,
            depthBuffer: false,
            stencilBuffer: false,
            premultipliedAlpha: false,
          }));
          planeMesh.material = materials['basicWhite'];
          planeMesh.material.map = textures['black'].texture;
          renderer.render(composeScene, cameraOrtho, positionFbo[i], true);
          planeMesh.material.map = startPositionsTexture;
          renderer.render(composeScene, cameraOrtho, edgeFbo[i], true);
        }
      }
      if ((now.renderVersion === 2 || now.renderVersion === 3) && !now.isInq) {
        noiseFbo = new THREE.WebGLRenderTarget(config.noiseSize, config.noiseSize, {
          minFilter: THREE.LinearFilter,
          magFilter: THREE.LinearFilter,
          format: THREE.RGBAFormat,
          wrapS: THREE.ClampToEdgeWrapping,
          wrapT: THREE.ClampToEdgeWrapping,
          type: glFeatures.singleFloatFbo ? THREE.FloatType : (glFeatures.halfFloatFbo ? THREE.HalfFloatType : THREE.UnsignedByteType),
          depthBuffer: false,
          stencilBuffer: false
        });
      } 
      if (now.renderVersion === 3 && !now.isInq) {
        rv3LinesFbo = new THREE.WebGLRenderTarget(config.rv3FboSide, config.rv3FboSide, {
          minFilter: THREE.LinearFilter,
          magFilter: THREE.LinearFilter,
          format: THREE.RGBFormat,
          wrapS: THREE.ClampToEdgeWrapping,
          wrapT: THREE.ClampToEdgeWrapping,
          depthBuffer: false,
          stencilBuffer: false,
          premultipliedAlpha: false,
        });
        for (var i = 0; i < 2; i++) {
          rv3LinesFbos[i] = new THREE.WebGLRenderTarget(config.rv3FboSide, config.rv3FboSide, {
            minFilter: THREE.LinearFilter,
            magFilter: THREE.LinearFilter,
            format: THREE.RGBFormat,
            wrapS: THREE.ClampToEdgeWrapping,
            wrapT: THREE.ClampToEdgeWrapping,
            depthBuffer: false,
            stencilBuffer: false,
            premultipliedAlpha: false,
          });
          planeMesh.material = materials['rv3Clone'].clone();
          planeMesh.material.map = textures['black'].texture;
          renderer.render(composeScene, cameraOrtho, rv3LinesFbos[i], true);
        }
        now.currentRV3FboIndex = 0;
      }

      bgFbo = new THREE.WebGLRenderTarget(config.bgFboSide, config.bgFboSide, {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.LinearFilter,
        format: THREE.RGBAFormat,
        wrapS: THREE.ClampToEdgeWrapping,
        wrapT: THREE.ClampToEdgeWrapping,
        type: glFeatures.singleFloatFbo ? THREE.FloatType : (glFeatures.halfFloatFbo ? THREE.HalfFloatType : THREE.UnsignedByteType),
        depthBuffer: false,
        stencilBuffer: false,
      });

      now.viewVTarget = now.viewV = currentState.viewV;
      now.viewHTarget = now.viewH = currentState.viewH;
      now.viewRTarget = now.viewR = currentState.viewR;      
      for (var stateName in _states) {
        if (!_states.hasOwnProperty(stateName)) continue;
        if (now.isinq) continue;
        var state = _states[stateName];
        var verticesArray = new Float32Array( config.dataFboSide * config.dataFboSide * 4 );
        var normalsArray = new Float32Array( config.dataFboSide * config.dataFboSide * 3 );
        if (state.type === 'model') {
          var vertexData = state.lines.rawImage.buffer;
          var normalsData = state.normals.rawImage.buffer;

          // Lines data
          var stride1 = config.dataFboSide * 4;
          var stride2 = config.dataFboSide * 3;
          for (var y = 0; y < config.dataFboSide; y++) {
            for (var x = 0; x < config.dataFboSide; x++) {
              var x4 = x * 4;
              var x3 = x * 3;
              var index1 = stride1 * y + x4;
              var index2 = stride2 * y + x3;
              var index3 = stride2 * (config.dataFboSide - 1 - y) + x3;
              verticesArray[index1] = vertexData[index1] / MAX_HALF_FLOAT;
              verticesArray[index1 + 1] = vertexData[index1 + 1] / MAX_HALF_FLOAT;
              verticesArray[index1 + 2] = vertexData[index1 + 2] / MAX_HALF_FLOAT;
              verticesArray[index1 + 3] = vertexData[index1 + 3] / MAX_HALF_FLOAT;
              normalsArray[index2] = normalsData[index3] / MAX_HALF_FLOAT;
              normalsArray[index2 + 1] = normalsData[index3 + 1] / MAX_HALF_FLOAT;
              normalsArray[index2 + 2] = normalsData[index3 + 2] / MAX_HALF_FLOAT;
            }
          }
        }
        if (state.type === 'displacementMap') {
          if (state.displacementAmount) {
            var data = state.lines.rawImage.buffer;
          } else {
            var data = null;
          }
          
          var stride1 = config.dataFboSide * 4;
          var stride2 = config.dataFboSide * 3;
          var stridePng = config.dataFboSide * 2;
          for (var y = 0; y < config.dataFboSide; y++) {
            for (var x = 0; x < config.dataFboSide; x++) {
              var x4 = x * 4;
              var x3 = x * 3;
              var x2 = x * 2;
              var index1 = stride1 * y + x4;
              var index2 = stride2 * y + x3;
              var index3 = stride2 * (config.dataFboSide - 1 - y) + x3;
              var indexPng = stridePng * y + x2;
              verticesArray[index1] = x / config.dataFboSide;
              if (state.displacementAmount) {
                verticesArray[index1 + 2] = 0.5 + data[indexPng] / MAX_HALF_FLOAT * state.displacementAmount;
              } else {
                verticesArray[index1 + 2] = 0.5;
              }
              verticesArray[index1 + 1] = (1.0 - y / config.dataFboSide);
              verticesArray[index1 + 3] = 1.0;

              normalsArray[index2] = 0.5;
              normalsArray[index2 + 1] = 0.5;
              normalsArray[index2 + 2] = 1;
            }
          }
        }

        var gradientTex = state.linesGradient ? state.textures['lines_gradient'] : textures['mask'].texture;
        state.linesGradientData = getImageData(gradientTex.image, config.dataFboSide, 1).data;

        if (state.type === 'background') {
          continue;
        }

        state.linesVerticesTexture = new THREE.DataTexture(
          verticesArray,
          config.dataFboSide,
          config.dataFboSide,
          THREE.RGBAFormat,
          floatType,
          null,
          THREE.MirroredRepeatWrapping,
          THREE.MirroredRepeatWrapping,
          THREE.LinearFilter,
          THREE.LinearFilter
        );
        state.linesVerticesTexture.needsUpdate = true;
        state.normalsTexture = new THREE.DataTexture(
          normalsArray,
          config.dataFboSide,
          config.dataFboSide,
          THREE.RGBFormat,
          floatType,
          null,
          THREE.MirroredRepeatWrapping,
          THREE.MirroredRepeatWrapping,
          THREE.LinearFilter,
          THREE.LinearFilter
        );
        state.normalsTexture.needsUpdate = true;
      }

      if (now.isInq) {
        var gridY1 = config.lineDotsNumber = 220;
        var gridY = gridY1 - 1;
        var instncesNumber = 50;
        var offsetAttribArray = new Float32Array( instncesNumber );
        for ( var i = 0; i < instncesNumber; i ++ ) {
          var x = i / (instncesNumber - 1);
          offsetAttribArray[ i ] = Math.random();
          // offsetAttribArray[ i ] = x * (Math.random() * 0.2 - 0.1);
        }
        var stripGeometry = new THREE.InstancedBufferGeometry();
        var varr = new Float32Array( gridY1 * 2 * 3);
        // var vdir = new Float32Array( gridY1 * 2 * 3);
        var larr = new Float32Array( gridY1 * 2 );
        var linew = 0.001;
        var offset = 0;
        var offsetl = 0;
        var segment_height = 1 / gridY;
        for ( var iy = 0; iy < gridY1; iy ++ ) {
          var y = iy * segment_height;
          var op = iy / gridY; 
          larr[ offsetl ] = op;
          larr[ offsetl + 1 ] = op;

          // larr[ offsetl ] = Math.sin(iy);
          // larr[ offsetl + 1 ] = Math.sin(iy);
          offsetl += 2;
          for ( var ix = 0; ix < 2; ix ++ ) {
            var x = ix * linew;
            varr[ offset ] = 0;
            varr[ offset + 1 ] = 0;
            varr[ offset + 2 ] = 0;
            offset += 3;
          }
        }

        stripGeometry.addAttribute( 'position', new THREE.BufferAttribute( varr, 3 ).setDynamic(true) );
        stripGeometry.addAttribute( 'offset_l', new THREE.BufferAttribute( larr, 1 ));
        // stripGeometry.addAttribute( 'offset_d', new THREE.BufferAttribute( vdir, 3 ).setDynamic(true));
        var is = new Uint16Array( gridY * 1 * 6 );
        offset = 0;
        for ( var iy = 0; iy < gridY; iy ++ ) {
            var a = 2 * iy;
            var b = 2 * ( iy + 1 );
            var c = 1 + 2 * ( iy + 1 );
            var d = 1 + 2 * iy;

            is[ offset ] = a;
            is[ offset + 1 ] = b;
            is[ offset + 2 ] = d;

            is[ offset + 3 ] = b;
            is[ offset + 4 ] = c;
            is[ offset + 5 ] = d;

            offset += 6;
        }
        stripGeometry.setIndex( new THREE.BufferAttribute( is, 1 ) );
        stripGeometry.addAttribute( 'offset_x', new THREE.InstancedBufferAttribute( offsetAttribArray, 1, 1 ));
        // linesMesh = new THREE.Mesh( stripGeometry,  new THREE.MeshBasicMaterial({color: 0xffffff}) );
        linesMesh = new THREE.Mesh( stripGeometry,  materials['inqDraw'] );
        // var geo = new THREE.PlaneBufferGeometry(1,1, 100, 100);
        // var pgeo = new THREE.InstancedBufferGeometry(1,1, 100, 100);
        // stripGeometry.addAttribute( 'offset_x', new THREE.InstancedBufferAttribute( offsetAttribArray, 1, 1 ));
        // linesMesh = new THREE.Mesh( new THREE.PlaneBufferGeometry(1,1, 100, 100),  new THREE.MeshBasicMaterial({color: 0xffffff}) );
        linesMesh.position.set(0, 0, 0);
        linesMesh.renderOrder = 100000;
        linesScene.add(linesMesh);
        linesMesh.frustumCulled = false;
        // --------------------------------------------------------------------------------------------------------------------------------------
      } else {
        // Lines variability 
        offsetDataShift = [];
        offsetDataSpeed = [];
        offsetArray = new Float32Array( config.lineInstancesNumber * 3 * 4 );
        var lineRange = 1 + 2 * currentState.lineLength;
        for (var x = 0; x < config.lineInstancesNumber; x++) {
          var x3 = x * 3;
          offsetDataShift[x] =  Math.random() * (1 + 2 * currentState.lineLength) - currentState.lineLength;
          offsetArray[x3] = Math.random();
          offsetArray[x3 + 1] = Math.random();
          offsetDataSpeed[x] = 0.008 + Math.random() * 0.008;
        }
        for (var i = 0; i < 50; i++) {
          var ind = (Math.random() * 0.7 + 0.15) * config.lineInstancesNumber;
          ind = Math.floor(ind) * 3;
          offsetArray[ind] = Math.random() * 0.5 + 0.5;
        }
        for (var i = 0; i < 20; i++) {
          var ind = (Math.random() * 0.3 + 0.35) * config.lineInstancesNumber;
          ind = Math.floor(ind) * 3;
          offsetArray[ind] = Math.random() * 0.5 + 0.5;
        }
        offsetDataTexture = new THREE.DataTexture(
          offsetArray,
          config.lineInstancesNumber,
          4,
          THREE.RGBFormat,
          floatType,
          null,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        offsetDataTexture.needsUpdate = true;

        // Lines mesh
        if (now.renderVersion <= 2) {
          config.lineDotsNumber = config.modelLineDotsNumber;
          if (now.renderVersion === 1) {
            config.lineDotsNumber = 50;
          }
          var offsetAttribArray = new Float32Array( config.lineInstancesNumber );
          for ( var i = 0; i < config.lineInstancesNumber; i ++ ) {
            var x = i / (config.lineInstancesNumber - 1);
            offsetAttribArray[ i ] = x;
          }
          var stripGeometry = new THREE.InstancedBufferGeometry();
          var varr = new Float32Array( config.lineDotsNumber * 12);
          var linew = 0.001;
          for ( var i = 0; i < config.lineDotsNumber; i ++ ) {
            var y = i / config.lineDotsNumber;
            var dy = 1 / config.lineDotsNumber;
            var num = i * 12;
            varr[ num ] = 0;
            varr[ num + 1 ] = y;
            varr[ num + 2] = 0.5;
            varr[ num + 3 ] = linew;
            varr[ num + 4 ] = y;
            varr[ num + 5 ] = 0.5;
            varr[ num + 6 ] = 0;
            varr[ num + 7 ] = y + dy;
            varr[ num + 8 ] = 0.5;
            varr[ num + 9 ] = linew;
            varr[ num + 10 ] = y + dy;
            varr[ num + 11 ] = 0.5;
          }
          stripGeometry.addAttribute( 'position', new THREE.BufferAttribute( varr, 3 ) );
          var is = new Uint16Array( config.lineDotsNumber * 2 * 3 );
          for ( var i = 0; i < config.lineDotsNumber; i ++ ) {
            var ind = i * 2 * 3;
            var vind = i * 4;
            is[ ind ] = vind;
            is[ ind + 1 ] = vind + 1;
            is[ ind + 2 ] = vind + 2;
            is[ ind + 3 ] = vind + 2;
            is[ ind + 4 ] = vind + 1;
            is[ ind + 5 ] = vind + 3;
          }
          stripGeometry.setIndex( new THREE.BufferAttribute( is, 1 ) );
          stripGeometry.addAttribute( 'offset_x', new THREE.InstancedBufferAttribute( offsetAttribArray, 1, 1 ));
          linesParent = new THREE.Object3D();
          linesParent.position.set(0, 0, 3);
          linesMesh = new THREE.Mesh( stripGeometry,  materials['linesDraw'] );
          linesMesh.position.set(0, 0, -3);
          linesMesh.renderOrder = 100000;
          linesParent.add(linesMesh);
          linesScene.add(linesParent);
        }

        if (now.renderVersion === 3) {
          rv3Edge = new Float32Array(config.rv3EdgeSide * 2);
          for (var i = 0; i < rv3Edge.length; i++) {
            rv3Edge[i] = 0;
          }
          rv3Geometry = new THREE.Geometry();
          var vsNum = 400;
          now.rv3Speed = []
          for ( i = 0; i < vsNum; i ++ ) {
            var y = 0.25 + Math.random() * 0.5;
            var vertex = new THREE.Vector3(i / vsNum, y, 0.5);
            rv3Geometry.vertices.push( vertex );
            now.rv3Speed[i] = Math.random() * 0.5 + 0.75;
          }

          linesMesh = new THREE.Points(rv3Geometry, materials['rv3Dots']);
          linesMesh.material.uniforms['uTex'].value = textures['dot'].texture;
          rv3Scene = new THREE.Scene();
          rv3Scene.add(linesMesh);

          var planeGeometry = new THREE.PlaneBufferGeometry(1, 1);
          rv3Plane = new THREE.Mesh(planeGeometry, materials['rv3Draw']);
          rv3Plane.position.set(0.5, 0.5, 0.5);
          rv3Plane.material.transparent = true;
          linesScene.add(rv3Plane);

          now.opacity = 0;
        }

        if (now.renderVersion === 4) {
          var planeGeometry = new THREE.PlaneBufferGeometry(1, 1);
          linesMesh = new THREE.Mesh(planeGeometry, materials['basicWhite'].clone());
          linesMesh.position.set(0.5, 0.5, 0.5);
          linesMesh.material.transparent = true;
          linesScene.add(linesMesh);
        }
      }

      // DEBUG
      // lines.debug.controls = new THREE.OrbitControls(camera, renderer.domElement);
      // lines.debug.controls.target.set(0.5, 0.5, 0.5);
      // lines.debug.controls.enableDamping = true;
      // lines.debug.controls.dampingFactor = 0.25;

      for (var animName in lines.animations) {
        lines.processAnimation(animName);
      }

      // lines.setPositions('default');
      now.renderState = currentState;

      lines.resize();
      lines.animate();
      lines.pause();
      if (now.renderVersion === 1) {
        lines.render(1050);
      }
      if (now.renderVersion === 2) {
        lines.render();
      }

      if (typeof options.onReady === 'function') {
        options.onReady(lines);
      }

      // DEBUG
      // window.addEventListener('keyup', function(e) {
      //   if (e.which === 50 && now.renderVersion === 1) { // 2
      //     // if (e.which === 50) { // 2
      //     lines.screenshot();
      //   }
      // });
    },
    processAnimation: function(animName) {
      if (!lines.animations.hasOwnProperty(animName)) return;
      var anim = lines.animations[animName];
      anim.startFrame = now.frame;
      anim[0].startFrame = 0;
      for (var i = 1; i < anim.length; i++) {
        anim[i].startFrame = anim[i - 1].startFrame + anim[i - 1].duration;
      }
    },
    debug: {
      k: 0,
      mask: 1,
      scroll: 0,
      twinkleMultiplier: 1,
      orbitControls: true,
      onKChange: function(value) {
        if (now.transitionActive) return;
        currentState = now.prevState || _states[Object.keys(_states)[0]];
        nextState = nextState || _states[Object.keys(_states)[2]];
        now.transitionK = value;
      },
      togglePass: function() {
        if (now._pass === PASS.COMPOSED) {
          now._pass = PASS.LINES;
          return;
        }
        if (now._pass === PASS.LIGHT) {
          now._pass = PASS.LINES;
          return;
        }
        if (now._pass === PASS.LINES) {
          now._pass = PASS.DOTS;
          return;
        }
        if (now._pass === PASS.DOTS) {
          now._pass = PASS.NOISE;
          return;
        }
        if (now._pass === PASS.NOISE) {
          now._pass = PASS.COMPOSED;
          return;
        }
      },
      invert: function() {
        lines.invert();
      }
    },
    render: function(fboSpeed) {
      if (now.isInq) {
        if (now.renderVersion < 4) lines.renderInquiry();
        return;
      }
      now.dxTarget = now.dxTarget || 0;
      now.dx = now.dx || 0;
      now.dx += (now.dxTarget - now.dx) * 0.05;
      if (now.renderVersion === 4) {
        if (now.fbImageText) {
          now.fbImageTop = now.h / 2 - now.dyWorld;
          now.fbImageDx = now.w / 2 + now.dx * 1000;
          if (now.fbImageTop !== now.fbImageTopPrev) {
            now.fbImageText.style.top = now.fbImageTop + 'px';
          }
          if (now.fbImageDx !== now.fbImageDxPrev) {
            now.fbImageText.style.left = now.fbImageDx + 'px';
          }
          now.fbImageTopPrev = now.fbImageTop;
          now.fbImageDxPrev = now.fbImageDx;
        }
        return;
      };
      raycaster.setFromCamera( new THREE.Vector2(now.x / now.w * 2 - 1, now.y / now.h * 2 - 1), camera );
      var intersects = raycaster.intersectObject( intersectPlaneMesh, false );
      now.intersectionPointTarget = intersects[0] && intersects[0].point;
      now.intersectionPoint = now.intersectionPoint || now.intersectionPointTarget;
      if (!now.intersectionPointTarget) {
        if (now.x / now.w < 0.5) {
          now.intersectionPointTarget = new THREE.Vector2(0, 0);
        } else {
          now.intersectionPointTarget = new THREE.Vector2(1, 0);
        }
      }
      if (now.intersectionPointTarget) {
        now.intersectionPoint.add(now.intersectionPointTarget.sub(now.intersectionPoint).multiplyScalar(0.1));
        now.intersectionPoint = new THREE.Vector2(now.intersectionPoint.x, now.intersectionPoint.y);        
      }
      var k = now.transitionK || 0;
      var kInv = 1 - k;

      now.bgActive = currentState.type === 'background' || nextState.type === 'background';

      lines.updateView();
      
      fboSpeed = fboSpeed || now.fboSpeed;
      var speed = 0.5;
      var nowT = new Date().getTime();
      var timer = now.frame / 60;
      var zNoise = 0;
      var animFactor = 0;
      var bgColor = new THREE.Vector3(0,0,0);

      now.dissolve = now.dissolve || 0;

      now.hoverImpactTarget = now.hoverImpactTarget || 0;
      now.hoverImpact = now.hoverImpact || 0;
      now.hoverImpact += (now.hoverImpactTarget - now.hoverImpact) * 0.05;

      now.pAveragePrev = now.pAverage || new THREE.Vector2(-1, -1);
      now.pAverage = new THREE.Vector2(0, 0);
      var total = 0;
      if (now.intersectionPoint) {
        pointers[pointers.length - 1] = now.intersectionPoint;
      }
      for (var i = 0; i < pointers.length - 1; i++) {
        if (pointers[i + 1]) {
          pointers[i] = pointers[i + 1];
          now.pAverage.add(pointers[i]);
          total++;
        }
      }

      now.dxScreenTarget = (now.x / now.w - .5) * 0.015;
      now.dyScreenTarget = (now.y / now.h - .5) * 0.015;
      now.dxScreen += (now.dxScreenTarget - now.dxScreen) * 0.04;
      now.dyScreen += (now.dyScreenTarget - now.dyScreen) * 0.04;
      
      if (now.renderVersion === 3) {
        rv3Plane.position.y = 0.5 - (now.dyWorld || 0) + now.dyScreen * 0.6;
        rv3Plane.position.x = 0.5 + now.dx - now.dxScreen;
      } else {
        if (now.renderVersion === 4) {
          linesMesh.position.y = 0.5 - (now.dyWorld || 0);
          linesMesh.position.x = 0.5 + now.dx;
        } else {
          now.pAverage.divideScalar(total);
          var dp = now.pAverage.clone().sub(now.pAveragePrev).length();
          now.pointerPowerTarget = clamp(1 - dp * 50, 0, 1) * 1;
          now.pointerPower = now.pointerPower || 0;
          now.pointerPower += (now.pointerPowerTarget - now.pointerPower) * 0.5;
          linesMesh.position.y = (- now.dyWorld || 0) + now.dyScreen * 0.6;
          linesMesh.position.x = now.dx - now.dxScreen;
        }
      }

      now.xScreenTarget = now.x / now.w || 0.5;
      now.xScreen = now.xScreen || 0.5;
      now.xScreen += (now.xScreenTarget - now.xScreen) * 0.05 * (1. - animFactor);

      if (now.bgActive) {
        var lineLength = now.renderState.lineLength;
        var diversity = now.renderState.diversity;
      } else {
        var lineLength = currentState.lineLength * kInv + nextState.lineLength * k;
        var diversity = currentState.diversity * kInv + nextState.diversity * k;
      }
      for (var x = 0; x < config.lineInstancesNumber; x++) {
        var x3 = x * 3;
        if (offsetDataShift[x] < - lineLength) {
          offsetDataShift[x] = 1 + lineLength;
        }
        offsetDataShift[x] = offsetDataShift[x] - speed * offsetDataSpeed[x];
        offsetArray[x3 + 1] = offsetDataShift[x];
      }
      var stride = 3 * config.lineInstancesNumber;
      var len = (config.lineInstancesNumber - 1) / (config.dataFboSide - 1);
      var gradData1 = currentState.linesGradientData || nextState.linesGradientData;
      var gradData2 = nextState.linesGradientData || currentState.linesGradientData;
      var colorK = Math.pow(k, 1);
      var colorKInv = 1 - colorK;
      if (now.prevColorK !== colorK || now.prevCurrentState !== currentState) {
        for (var x = 0; x < config.lineInstancesNumber; x++) {
          var i = Math.floor(x / len);
          var x3 = x * 3;
          var i4 = i * 4;
          var index = stride + x3;
          offsetArray[index] = (gradData1[i4] * colorKInv + gradData2[i4] * colorK) / 255.0;
          offsetArray[index + 1] = (gradData1[i4 + 1] * colorKInv + gradData2[i4 + 1] * colorK) / 255.0;
          offsetArray[index + 2] = (gradData1[i4 + 2] * colorKInv + gradData2[i4 + 2] * colorK) / 255.0;
        }
      }
      now.prevColorK = colorK;
      now.prevCurrentState = currentState;
      if (now.renderVersion === 2) offsetDataTexture.needsUpdate = true;

      if (lines.debug.controls) {
        lines.debug.controls.enabled = true;
      } else {
        var camPos;
        var lookAt;
        var up = new THREE.Vector3(0, 1, 0);
        var fov = config.fov;
        if (now.animation) {
          var animStates = lines.animations[now.animation];
          var animFrame = now.frame - animStates.startFrame;
          var currentAnimState;
          var nextAnimState = null;
          var maxLeftFrame = -1;
          var minRightFrame = Number.MAX_VALUE;
          for (var i = 0; i < animStates.length; i++) {
            if (animFrame >= animStates[i].startFrame && animStates[i].startFrame > maxLeftFrame) {
              maxLeftFrame = animStates[i].startFrame;
              currentAnimState = animStates[i];
            }
            if (animFrame < animStates[i].startFrame && animStates[i].startFrame < minRightFrame) {
              minRightFrame = animStates[i].startFrame;
              nextAnimState = animStates[i];
            }
          }
          fov = currentAnimState.fov;
          if (nextAnimState) {
            var ak = (animFrame - currentAnimState.startFrame) / (nextAnimState.startFrame - currentAnimState.startFrame);
            if (!currentAnimState.opacity) {
              now.bgDissolve = 1 - Math.pow(ak, 0.85);
              var hiddenK = 0.06;
              var kActive = 1 - hiddenK;
              if (ak < hiddenK) {
                now.opacity = 0;
              } else {
                now.opacity = easings['easeOutQuart'](null, ak - hiddenK, 0, 1, kActive);
                now.opacity = Math.pow(now.opacity, 0.7);
              }
            } else {
              now.opacity = currentAnimState.opacity;
              now.bgDissolve = 0;
            }
            
            ak = easings[currentAnimState.easing](null, ak, 0, 1, 1);
            ak = Math.pow(ak, 1.1);
            if (ak > 0.9) {
              now.introShowed = true;
            }
            var akInv = 1 - ak;
            var akNoise = Math.pow(ak, .2);
            var akInvNoise = 1 - akNoise;
            camPos = currentAnimState.cameraPos.clone().multiplyScalar(akInv).add(nextAnimState.cameraPos.clone().multiplyScalar(ak));
            lookAt = currentAnimState.lookAt.clone().multiplyScalar(akInv).add(nextAnimState.lookAt.clone().multiplyScalar(ak));
            zNoise = currentAnimState.zNoise * akInvNoise + nextAnimState.zNoise * akNoise;
            now.surrounding = currentAnimState.surrounding * akInv + nextAnimState.surrounding * ak;
            animFactor = Math.pow(akInv, 1.);
            up = currentAnimState.up.clone().multiplyScalar(akInv).add(nextAnimState.up.clone().multiplyScalar(ak)).normalize();
          } else {
            now.animation = null;
          }
        } else {
          var dH = (now.x / now.w - 0.5)  * 0.08 || 0;
          var dV = (now.y / now.h - 0.5) * 0.06 || 0;
          var pointerImpact = currentState.pointerImpact * kInv + nextState.pointerImpact * k;
          dH *= pointerImpact;
          dV *= pointerImpact;
          if (now.dissolving) {
            dH = dV = 0;
          }
          now.viewH += (now.viewHTarget - dH - now.viewH) * config.inertialK;
          now.viewV += (now.viewVTarget - dV - now.viewV) * config.inertialK;
          now.viewR += (now.viewRTarget - now.viewR) * config.inertialK;
          var vz = now.viewR * Math.sin(now.viewV) * Math.cos(now.viewH);
          var vx = now.viewR * Math.sin(now.viewV) * Math.sin(now.viewH);
          var vy = now.viewR * Math.cos(now.viewV);
          camPos = new THREE.Vector3(vx, vy, vz).add(new THREE.Vector3(0.5, 0.5, 0.5));
          lookAt = new THREE.Vector3(0.5, 0.5, 0.5);
        }
        if (camPos) {
          var dz = camPos.z - 0.5;
          var zmul = camera.aspect / (15 / 9);
          if (now.renderVersion !== 2) {
            dz /= Math.pow(zmul, 0.9);
          }
          camPos.z = 0.45 + dz;
          camera.position.set(camPos.x, camPos.y, camPos.z);
          camera.up.copy(up);
          camera.lookAt(lookAt);
          camera.fov = fov;
          camera.updateProjectionMatrix();

        }
      }

      var linesColor = currentState.linesColor.clone().lerp(nextState.linesColor, k);

      if (now.renderVersion === 1) {
        var noiseTimer = now.noiseFrame / 60;
        planeMesh.material = materials['noise'];
        planeMesh.material.uniforms['uTime'].value = noiseTimer * 0.07 * 0.3 + now.seed;
        planeMesh.material.uniforms['uScale'].value = 11;
        renderer.render(composeScene, cameraOrtho, noiseFbo_LE2, true);
        now.noiseFrame++;
        var posFboRes = new THREE.Vector2(config.positionFboSide, config.positionFboSide);
        for (var i = 0; i < fboSpeed; i++) {
          now.currentPosFboIndex = 1 - now.currentPosFboIndex;
          var targetIndex;
          var sourceIndex;
          targetIndex = now.currentPosFboIndex;
          sourceIndex = 1 - targetIndex;
          planeMesh.material = materials['edgeFbo'];
          planeMesh.material.uniforms['uEdgeTex'].value = edgeFbo[sourceIndex];
          planeMesh.material.uniforms['uPositionTex'].value = positionFbo[sourceIndex];
          planeMesh.material.uniforms['uVertTex1'].value = now.renderState.linesVerticesTexture;
          planeMesh.material.uniforms['uZNoise'].value = zNoise;
          planeMesh.material.uniforms['uTime'].value = timer * speed;
          planeMesh.material.uniforms['uNoiseAmount'].value = now.renderState.noiseAmount;
          if (now.frame < 2) {
            planeMesh.material.uniforms['uEdgeInitialTex'].value = initialPositionsTexture;
            planeMesh.material.uniforms['uEdgeDataTex'].value = positionDataTexture;
            planeMesh.material.uniforms['uNoiseTex'].value = noiseFbo_LE2;
            planeMesh.material.uniforms['uResolution'].value = posFboRes;
            planeMesh.material.uniforms['uEdgeResolution'].value = new THREE.Vector2(config.edgeFboSide, config.edgeFboSide);
          }
          renderer.render(composeScene, cameraOrtho, edgeFbo[targetIndex], true);
          planeMesh.material = materials['positionFbo'];
          planeMesh.material.uniforms['uPositionTex'].value = positionFbo[sourceIndex];
          planeMesh.material.uniforms['uEdgeTex'].value = edgeFbo[sourceIndex];
          planeMesh.material.uniforms['uvStepAdd'].value = now.dissolve * 0.1;
          if (now.frame < 2) {
            planeMesh.material.uniforms['uEdgeFboSide'].value = config.edgeFboSide;
            planeMesh.material.uniforms['uResolution'].value = posFboRes;
            planeMesh.material.uniforms['uEdgeDataTex'].value = positionDataTexture;
            planeMesh.material.uniforms['uEdgeInitialTex'].value = initialPositionsTexture;
          }
          renderer.render(composeScene, cameraOrtho, positionFbo[targetIndex], true);
        }

        var _thickness = kInv * currentState.thickness + k * nextState.thickness;
        if (now.animation) {
          _thickness *= (1. - animFactor * 0.4);
        }

        linesMesh.material = materials['linesDraw'];
        linesMesh.material.uniforms['uOffsetTex'].value = offsetDataTexture;
        linesMesh.material.uniforms['uNoiseTex'].value = noiseFbo_LE2;
        linesMesh.material.uniforms['uCameraPos'].value = camera.position;
        linesMesh.material.uniforms['uThickness'].value = _thickness;
        linesMesh.material.uniforms['uPointerXScreen'].value = now.xScreen;
        linesMesh.material.uniforms['uPointerPower'].value = now.pointerPower;
        linesMesh.material.uniforms['uDissolve'].value = now.dissolve;
        linesMesh.material.uniforms['uDy'].value = now.dy + now.hoverImpact * 0.65 || 0;
        linesMesh.material.uniforms['uPointer'].value = now.intersectionPoint ? now.intersectionPoint : new THREE.Vector2(-1, -1);
        linesMesh.material.uniforms['uOpacity'].value = now.opacity;
        linesMesh.material.uniforms['uAnimFactor'].value = animFactor;
        linesMesh.material.uniforms['uLinesColor'].value = linesColor;
        linesMesh.material.uniforms['uPosTex1'].value = positionFbo[now.currentPosFboIndex];
        linesMesh.material.uniforms['uMaskTex1'].value = now.renderState.textures['mask'] || textures['mask'].texture;
        linesMesh.material.uniforms['uSurrounding'].value = kInv * currentState.surrounding + k * nextState.surrounding;
      }
      if (now.renderVersion === 2) {
        if (now.bgActive) {
          var _currentState = now.renderState;
          var _nextState = now.renderState;
          var _k = 0;
          var _kInv = 1;
          lineLength = now.renderState.lineLength;
          diversity = now.renderState.diversity;
        } else {
          var _currentState = currentState;
          var _nextState = nextState;
          var _k = k;
          var _kInv = kInv;
        }
        // var lightDir = new THREE.Vector3(-vx * 2, -vy, vz).multiplyScalar(1);
        planeMesh.material = materials['noise_LE1'];
        planeMesh.material.uniforms['uTime'].value = timer * 0.04 + now.seed;
        planeMesh.material.uniforms['uScale'].value = 3;
        renderer.render(composeScene, cameraOrtho, noiseFbo, true);
        var currentLightDir = _currentState.lightDirection ? _currentState.lightDirection.clone() : new THREE.Vector3(0,0,1);
        var nextLightDir = _nextState.lightDirection ? _nextState.lightDirection.clone() : new THREE.Vector3(0,0,1);
        linesMesh.material = materials['linesDraw_LE1'];
        var lightDir = currentLightDir.multiplyScalar(_kInv).add(nextLightDir.multiplyScalar(_k));
        lightDir.x = lightDir.x + (now.x / now.w - 0.5) * 3.;
        lightDir.y = lightDir.y + (now.y / now.h - 0.5) * 2.;
        linesMesh.material.uniforms['uOffsetTex'].value = offsetDataTexture;
        linesMesh.material.uniforms['uNoiseTex'].value = noiseFbo;
        linesMesh.material.uniforms['uLightDir'].value = lightDir.normalize();
        linesMesh.material.uniforms['uTime'].value = timer * speed;
        linesMesh.material.uniforms['uThickness'].value = _kInv * _currentState.thickness + _k * _nextState.thickness;
        linesMesh.material.uniforms['uMovement'].value = _kInv * _currentState.movement + _k * _nextState.movement;
        linesMesh.material.uniforms['uPointerX'].value = now.intersectionPoint ? now.intersectionPoint.x : -1;
        linesMesh.material.uniforms['uTwinkleMultiplier'].value = _kInv * _currentState.twinkleMultiplier + _k * _nextState.twinkleMultiplier;
        linesMesh.material.uniforms['uZeroZ1'].value = _currentState.zZero;
        linesMesh.material.uniforms['uZeroZ2'].value = _nextState.zZero;
        linesMesh.material.uniforms['uOneZ1'].value = _currentState.zOne;
        linesMesh.material.uniforms['uOneZ2'].value = _nextState.zOne;
        linesMesh.material.uniforms['uOpacity'].value = now.opacity;
        linesMesh.material.uniforms['uDiversity'].value = diversity;
        linesMesh.material.uniforms['uLineLength'].value = lineLength;
        linesMesh.material.uniforms['uVertTex1'].value = _currentState.linesVerticesTexture;
        linesMesh.material.uniforms['uVertTex2'].value = _nextState.linesVerticesTexture;
        linesMesh.material.uniforms['uNormTex1'].value = _currentState.normalsTexture;
        linesMesh.material.uniforms['uNormTex2'].value = _nextState.normalsTexture;
        linesMesh.material.uniforms['uMaskTex1'].value = _currentState.textures['mask'] || textures['mask'].texture;
        linesMesh.material.uniforms['uMaskTex2'].value = _nextState.textures['mask'] || textures['mask'].texture;
        // linesMesh.material.uniforms['uTransitionTex'].value = transitions[now.transition].texture;
        linesMesh.material.uniforms['k'].value = _k;
        linesMesh.material.uniforms['uMultiplier'].value = _kInv * _currentState.colorMultiplier + _k * _nextState.colorMultiplier;
        linesMesh.material.uniforms['uSurrounding'].value = _kInv * _currentState.surrounding + _k * _nextState.surrounding;
        linesMesh.material.uniforms['uLightIntensity'].value = _kInv * _currentState.lightIntensity + _k * _nextState.lightIntensity;
      }

      var bgSpeed = .7;
      var bgAngleNoise = noise.perlin2(timer * 0.6 * bgSpeed, 1);
      var bgXNoise = noise.perlin2(timer * 0.4 * bgSpeed, 5);
      var bgScaleNoise = noise.perlin2(timer * 0.9 * bgSpeed, 8);
      
      var bgMat = materials['background'];
      bgMat.uniforms['uGradientTex1'].value = currentState.textures['bg_gradient'] || textures['black'].texture;
      bgMat.uniforms['uGradientTex2'].value = nextState.textures['bg_gradient'] || textures['black'].texture;
      bgMat.uniforms['uAngle'].value = kInv * currentState.backgroundAngle + k * nextState.backgroundAngle + now.dx * 3.5;
      bgMat.uniforms['uScale'].value = kInv * currentState.backgroundScale + k * nextState.backgroundScale;
      bgMat.uniforms['uDynamics'].value = kInv * currentState.backgroundDynamics + k * nextState.backgroundDynamics;
      bgMat.uniforms['uInverseTransition'].value = nextState.bgTransitionInverted;
      bgMat.uniforms['uHue'].value = 0;
      // bgMat.uniforms['uSat'].value = now.bgSat;
      bgMat.uniforms['uSat'].value = 1;
      bgMat.uniforms['uValue'].value = 0;
      bgMat.uniforms['uXShift'].value = now.dx * 2. || 0;
      bgMat.uniforms['uYShift'].value = now.dy * 0.15 || 0;
      bgMat.uniforms['k'].value = k;
      bgMat.uniforms['uTime'].value = timer * speed;
      bgMat.uniforms['uAngleNoise'].value = bgAngleNoise * (1 - now.dy);
      bgMat.uniforms['uXNoise'].value = bgXNoise * (1 - now.dy);
      bgMat.uniforms['uScaleNoise'].value = bgScaleNoise * (1 - now.dy);
      bgMat.uniforms['uDissolve'].value = now.bgDissolve;
      bgMat.uniforms['uColor'].value = now.bgColor;
      if (!now.animation) {
        now.bgOpacity = 1 - now.bgDissolve;
      }
      bgMat.uniforms['uOpacity'].value = now.bgOpacity;
      bgMat.uniforms['uOpacity'].value = 1;

      bgPlaneMesh.scale.y = 1.9 * Math.pow(camera.fov / 50, 1.27);
      bgPlaneMesh.scale.x = bgPlaneMesh.scale.y * camera.aspect;

      planeMesh.material = materials['background'];
      renderer.render(composeScene, cameraOrtho, bgFbo, true);

      bgPlaneMesh.material.map = bgFbo;

      if (now.bgTransition) {
        
        if (now.renderVersion === 3) {
          rv3Plane.visible  = false;
        } else {
          linesMesh.visible = false;
        }
      } else {
        
        if (now.renderVersion === 3) {
          rv3Plane.visible  = true;
        } else {
          linesMesh.visible = true;
        }
      }

      if (now._pass === PASS.COMPOSED) {
        if (now.renderVersion === 3) {
          now.opacity += 0.01;
          if (now.opacity > 1) now.opacity = 1;
          renderer.setClearColor( new THREE.Color(0x000000), 0);
          planeMesh.material = materials['noise_LE1'];
          planeMesh.material.uniforms['uTime'].value = timer * 0.1 + now.seed;
          planeMesh.material.uniforms['uScale'].value = 12;
          renderer.render(composeScene, cameraOrtho, noiseFbo, true);
          lines.renderRV3(2, timer, now.frame);
          rv3Plane.material.uniforms['uTex'].value = rv3LinesFbos[now.currentRV3FboIndex] || textures['black'].texture;
          rv3Plane.material.uniforms['uNoiseTex'].value = noiseFbo || textures['black'].texture;
          rv3Plane.material.uniforms['uMaskTex'].value = now.renderState.textures['mask'] || textures['mask'].texture;
          rv3Plane.material.uniforms['uLinesColor'].value = linesColor;
          rv3Plane.material.uniforms['uOpacity'].value = (1 - now.dy) * now.opacityMul * now.opacity - now.hoverImpact * 0.7;
          renderer.render(linesScene, camera);
        } else if (now.renderVersion === 4) {
          linesMesh.material.map = now.renderState.textures['rv3'];
          renderer.setClearColor( new THREE.Color(0x000000), 0 );
          renderer.render(linesScene, camera);
        } else {
          renderer.setClearColor( new THREE.Color(0x000000), 0 );
          renderer.render(linesScene, camera);
        }
      }

      callbacks['ondraw'](currentState);

      now.resized = false;
      now.frame++;
    },
    renderInquiry: function() { // ------------------------------------------------------------------------------------------------------------------------
      var timer = now.frame / 60;
      var speed = 0.5;
      var bgSpeed = .5;
      var bgAngleNoise = noise.perlin2(timer * 0.6 * bgSpeed, 1);
      var bgXNoise = noise.perlin2(timer * 0.4 * bgSpeed, 5);
      var bgScaleNoise = noise.perlin2(timer * 0.9 * bgSpeed, 8);
      var bgValNoise = noise.perlin2(timer * 0.2 * bgSpeed, 8);

      if (typeof now.inqK !== 'number') {
        now.bgCurrent = lines.selectBgVariation(currentState);
        now.bgNext = lines.selectBgVariation(currentState);
        now.inqK = 0;
      }
      var k = 0;
      if (now.inqK > 1) {
        now.inqK = 1;
      }
      if (now.frame % 900 === 0) {
        now.bgCurrent = now.bgNext;
        now.bgNext = lines.selectBgVariation(currentState);
        now.inqK = 0;
      }
      now.inqK += speed * 0.003;

      var linesU = linesMesh.material.uniforms;
      linesU['uLinesColor'].value = new THREE.Vector3(0xffffff);
      linesU['uOpacity'].value = 1;
      linesU['uTime'].value = timer;
      
      var bgMat = materials['background'];
      bgMat.uniforms['uGradientTex1'].value = now.bgCurrent || textures['black'].texture;
      bgMat.uniforms['uGradientTex2'].value = now.bgNext || textures['black'].texture;
      bgMat.uniforms['uAngle'].value = currentState.backgroundAngle;
      bgMat.uniforms['uScale'].value = currentState.backgroundScale;
      bgMat.uniforms['uDynamics'].value = currentState.backgroundDynamics + now.bgFlick * 20;
      bgMat.uniforms['uInverseTransition'].value = currentState.bgTransitionInverted;
      bgMat.uniforms['uHue'].value = bgXNoise * 0.02;
      bgMat.uniforms['uSat'].value = 1;
      // bgMat.uniforms['uValue'].value = now.bgFlick * 0.5;
      bgMat.uniforms['uValue'].value = 0;
      bgMat.uniforms['uXShift'].value = 0;
      bgMat.uniforms['uYShift'].value = bgValNoise * 0.2 + 0.05;
      bgMat.uniforms['k'].value = now.inqK;
      bgMat.uniforms['uTime'].value = timer * speed;
      bgMat.uniforms['uAngleNoise'].value = bgAngleNoise * 1.5;
      bgMat.uniforms['uXNoise'].value = bgXNoise;
      bgMat.uniforms['uScaleNoise'].value = bgScaleNoise;
      bgMat.uniforms['uDissolve'].value = 0;
      bgMat.uniforms['uColor'].value = now.bgColor;
      bgMat.uniforms['uOpacity'].value = 1;

      bgPlaneMesh.scale.y = 1.9 * Math.pow(camera.fov / 50, 1.27);
      bgPlaneMesh.scale.x = bgPlaneMesh.scale.y * camera.aspect;
      planeMesh.material = materials['background'];
      renderer.render(composeScene, cameraOrtho, bgFbo, true);
      bgPlaneMesh.material.map = bgFbo;

      lines.updateInq();

      lines.updateView();

      renderer.render(linesScene, camera);

      callbacks['ondraw'](currentState);

      now.resized = false;
      now.frame++;
    },
    move: function(amount) {
      if (!now.isInq) return;
      amount = amount || 10;

      var ds = 13;
      var sMax = 20;
      var bMax = 27;
      now.inqSpeed += ds;
      now.bgSpeed += ds;
      if (now.inqSpeed > sMax) {
        now.inqSpeed = sMax;
      }
      // if (now.bgSpeed > bMax) {
      //   now.bgSpeed = bMax;
      // }
    },
    updateInq: function() {
      now.inqSpeed = now.inqSpeed || 1;
      // now.bgSpeed = now.bgSpeed || 0;
      now.inqSpeedTarget = 1;
      // now.bgSpeedTarget = 0;

      // now.bgFlickVTarget = now.bgFlickVTarget || 0;
      // now.bgFlickV = now.bgFlickV || 0;

      // now.bgFlickV += (now.bgFlickTarget - now.bgFlickV) * 0.05;

      // now.bgFlick = (noise.perlin2(0.123, now.inqFrame * 0.1) + 1) * 0.001 * now.bgSpeed;

      now.inqSpeed += (now.inqSpeedTarget - now.inqSpeed) * 0.1;
      // now.bgSpeed += (now.bgSpeedTarget - now.bgSpeed) * 0.05;

      now.inqFrame = now.inqFrame || 10;

      now.inqOffset0 = now.inqOffset0 || (1 + Math.random() * now.seed * 0.1);
      now.inqOffset1 = now.inqOffset1 || (3 + Math.random() * now.seed * 0.1);
      now.inqOffset2 = now.inqOffset2 || (5 + Math.random() * now.seed * 0.1);

      var varr = linesMesh.geometry.attributes.position.array;
      var gridY = config.lineDotsNumber - 1;
      var gridY1 = gridY + 1;
      var linew = 0.005;
      var offset = 0;
      now.v3 = now.v3 || new THREE.Vector3();
      var dir = new THREE.Vector3();
      now.inqDir = now.inqDir || new THREE.Vector3(1,1,1);

      var p1 = now.inqFrame * 0.005;
      dir.x = noise.perlin2(0.123, now.inqOffset0 + p1);
      dir.y = noise.perlin2(0.123, now.inqOffset1 + p1);
      dir.z = noise.perlin2(0.123, now.inqOffset2 + p1);
      dir.y += 0.05;
      dir.normalize();

      now.inqDir.add(dir.multiplyScalar(0.0001 * now.inqSpeed));
      now.inqDir.normalize();

      var speed = 0.02 ;
      var newV3 = now.v3.clone().add(now.inqDir.multiplyScalar(speed));
      for ( var iy = 0; iy < gridY; iy ++ ) {
        for ( var ix = 0; ix < 2; ix ++ ) {
          varr[ offset ] = varr[ offset + 6 ];
          varr[ offset + 1 ] = varr[ offset + 7 ];
          varr[ offset + 2 ] = varr[ offset + 8 ];
          offset += 3;
        }
      }
      varr[ offset ] = newV3.x;
      varr[ offset + 1] = newV3.y;
      varr[ offset  + 2] = newV3.z;
      var toCam = camera.position.clone().sub(newV3);
      var toPrev = now.v3.clone().sub(newV3);
      var v3Side = newV3.clone().add(toCam.cross(toPrev).normalize().multiplyScalar(linew));
      v3Side = newV3.clone();
      v3Side.x += linew;
      varr[ offset + 3] = v3Side.x;
      varr[ offset + 4] = v3Side.y;
      varr[ offset  + 5] = v3Side.z;

      now.v3Prev = now.v3.clone();
      now.v3 = newV3.clone();

      linesMesh.geometry.attributes.position.needsUpdate = true;
      now.inqFrame += 2;
    },
    renderRV3: function(times, timer, frame) {
      for (var t = 0; t < times; t++) {
        var vs = linesMesh.geometry.vertices;
        var vec;
        var angle;
        var noiseK = 0.0;
        var dvDef = new THREE.Vector3(0, 1, 0);
        var nScale = 14;
        var dt = timer * 0.01 + now.seed * 0.01;
        for (var i = 0; i < vs.length; i++) {
          vec = vs[i];
          var px0 = vec.x * nScale + dt;
          var py0 = vec.y * nScale + dt;
          var n1 = noise.perlin2(px0, py0);
          angle = (n1) * Math.PI / 2.5;
          var dv = new THREE.Vector3(Math.sin(angle), Math.cos(angle), 0);
          dv.lerp(dvDef, noiseK).multiplyScalar(0.0009 * now.rv3Speed[i]);
          vec.add(dv);
          var vk = (1. - Math.pow(Math.abs(vec.y - 0.7), 0.1)) * 0.05;
          vec.x = vec.x * (1 - vk) + i / vs.length * vk;
          if (vec.y > 0.75) {
            vec.x = i / vs.length;
            vec.y = 0.25;
          }
        }
        now.currentRV3FboIndex = 1 - now.currentRV3FboIndex;
        linesMesh.geometry.verticesNeedUpdate = true;
        renderer.render(rv3Scene, cameraOrtho, rv3LinesFbo, true);
        var rv3TargetIndex = now.currentRV3FboIndex;
        var rv3SourceIndex = 1 - rv3TargetIndex;
        planeMesh.material = materials['rv3Grow'];
        var sub = frame % (isTouchDevice ? 3 : 4) ? 0.004 : 0;
        planeMesh.material.uniforms['uSub'].value = sub;
        planeMesh.material.uniforms['uPrevTex'].value = rv3LinesFbos[rv3SourceIndex];
        planeMesh.material.uniforms['uEdgeTex'].value = rv3LinesFbo;
        renderer.render(composeScene, cameraOrtho, rv3LinesFbos[rv3TargetIndex], true);
      }
    },
    updateView: function() {
      if (now.isInq) {
        now.v3Prev = now.v3Prev || new THREE.Vector3();
        now.v3 = now.v3 || new THREE.Vector3(1,1,1);
        now.lookAt = now.lookAt || now.v3.clone();
        var toPrev = now.v3Prev.clone().sub(now.v3).normalize();
        var up = new THREE.Vector3(0,1,0);
        now.viewRTarget = 0.7 ;
        now.viewHTarget = Math.sin(now.frame * 0.001);
        now.viewVTarget = 0.5;
        now.lookAt.add(now.v3.clone().sub(now.lookAt).multiplyScalar(0.05));
        now.viewH += (now.viewHTarget - now.viewH) * 0.05;
        now.viewV += (now.viewVTarget - now.viewV) * 0.05;
        now.viewR += (now.viewRTarget - now.viewR) * 0.05;
        var vz = now.viewR * Math.sin(now.viewV) * Math.cos(now.viewH);
        var vx = now.viewR * Math.sin(now.viewV) * Math.sin(now.viewH);
        var vy = now.viewR * Math.cos(now.viewV);
        var camPos = new THREE.Vector3(vx, vy, vz).add(now.lookAt);
        camera.position.copy(camPos);
        camera.lookAt(now.lookAt);
      } else {        
        var k = now.transitionK || 0;
        if (currentState.type !== 'background' && nextState.type !== 'background') {
          now.viewRTarget = currentState.viewR * (1 - k) + nextState.viewR * k;
          now.viewHTarget = currentState.viewH * (1 - k) + nextState.viewH * k;
          now.viewVTarget = currentState.viewV * (1 - k) + nextState.viewV * k;
        } else {
          now.viewRTarget = now.renderState.viewR;
          now.viewHTarget = now.renderState.viewH;
          now.viewVTarget = now.renderState.viewV;
        }
      }
    },
    animate: function() {
      rafInstance = window.requestAnimationFrame(lines.animate);
      if (!now.active && !now.needsUpdate) {
        return;
      }
      if (now.needsUpdate) {
        now.needsUpdate = false;
      }
      if (!now.needsUpdate && now.needsPause) {
        now.bgTransition = false;
      }
      if (!now.animationtarted && now.renderVersion === 1) {
        now.animationtarted = true;
        lines.animation(currentState.name);
      }
      if (now.transitionActive) {
        var nowT = new Date().getTime();
        now.transitionT = nowT - now.transitionStartT;
        if (now.inverse) {
          var t = now.transitionFinishT - nowT;
        } else {
          var t = now.transitionT;
        }
        now.transitionK = now.easing(null, t, 0, 1, now.transitionFinishT - now.transitionStartT);
        lines.debug.k = now.transitionK;

        if (now.bgTransition) {
          now.bgDissolve = Math.cos(now.transitionK * Math.PI * 2) * 0.5 + 0.5;
          now.bgColor = now.bgColor0.clone().lerp(now.bgColor1, now.transitionK);
          // now.bgSat = 0;
        }
        
        if (now.transitionT > now.transitionFinishT - now.transitionStartT) {
          now.transitionActive = false;
          lines.debug.k = 0;
          now.transitionK = null;
          if (!now.inverse) {
            currentState = nextState;
          }
          now.inverse = false;
          if (fsm.transition) {
            fsm.transition();
          }
          if (now.deferredState) {
            lines.transition({
              type: 'to-' + now.deferredState.name,
              duration: now.deferredDuration,
              transition: now.deferredTransition,
              easing: now.deferredEasing
            });
            now.deferredState = null;
          }
          if (now.bgTransition) {
            now.bgDissolve = 1;
            now.bgColor = now.bgColor1;
            // now.bgSat = 1;
          }
          if (now.onCompleteTransition) {
            now.onCompleteTransition();
            now.onCompleteTransition = null;
          }
          // now.bgTransition = false;
        }

        lines.updateView();
        callbacks['ontransition'](currentState.name, nextState.name, now.transitionK);
      }

      if (now.dissolving) {
        var nowT = new Date().getTime();
        var dissolveT = nowT - now.dissolveStartT;

        if (nowT < now.dissolveFinishT) {
          var t = nowT - now.dissolveStartT;
          var dt = now.dissolveFinishT - now.dissolveStartT;
          var dyLeft = 1 - now.dyStart;
          var ease = now.bgTransition ? 'easeInOutQuad' : 'easeOutQuad';
          if (now.dissolveInvert) {
            var dk = easings[ease](null, t, 0, 1, dt);
            now.dy = 1 - dk;
            now.bgDissolve = 1 - dk;
          } else {
            var dk = easings[ease](null, t, 0, 1, dt);
            now.dy = now.dyStart + dyLeft * dk;
            now.bgDissolve = dk;
          }
          
          if (now.renderVersion === 4) {
            lines.canvas.style.opacity = 1 - now.bgDissolve;
            now.fbImageText.style.opacity = 1 - now.bgDissolve;
          } else if (now.bgTransition) {
            now.bgColor = now.bgColor0.clone().lerp(now.bgColor1, dk);
          }
        }

        if (nowT > now.dissolveFinishT) {
          now.dissolving = false;
          if (now.dissolveInvert) {
            now.bgDissolve = 0;
            now.dy = 0;
          } else {
            now.bgDissolve = 1;
            now.dy = 1;
          }
          if (now.bgTransition) {
            now.bgColor = now.bgColor1;
          }
          now.bgTransition = false;
          now.onDissolveComplete && now.onDissolveComplete();
          if (now.renderVersion === 4) {
            if (now.dissolveInvert) {
              lines.canvas.style.opacity = 1;
              now.fbImageText.style.opacity = 1;
            } else {
              lines.canvas.style.opacity = 0;
              now.fbImageText.style.opacity = 0;
            }
          }
        }
      }

      // DEBUG
      if (now.renderVersion < 4 && lines.debug.controls) {
        lines.debug.controls.update();
      }

      if (!now.needsUpdate && now.needsPause) {
        now.needsPause = false;
        now.bgTransition = true;
        lines.pause();
      }

      lines.render();

    },
    transition: function(params) {
      if (now.dissolving) return;
      var type = params.type || '';
      var duration = params.duration;
      if (now.renderVersion === 4) {
        duration = 1;
      }
      var transition = params.transition;
      var easing = params.easing;
      var interruptionMode = params.interruptionMode;
      var forced = params.forced;
      var fromState = params.from;
      type = type.replace('-', '').toLowerCase();
      var sname = type.substr(2);
      if (fromState === 'work' && sname === 'about' || fromState === 'about' && sname === 'work') {
        if (sname === 'about') {
          now.bgColor0 = new THREE.Color(0x000000);
          now.bgColor1 = new THREE.Color(0xffffff);
          if (now.renderVersion === 4) {
            config.canvas.style.backgroundImage = '';
            config.canvas.style.backgroundColor = '#ffffff';
            now.fbImageText.style.opacity = 0;
          }
        }
        if (sname === 'work') {
          now.bgColor0 = new THREE.Color(0xffffff);
          now.bgColor1 = new THREE.Color(0x000000);
          if (now.renderVersion === 4) {
            config.canvas.style.backgroundImage = '';
            config.canvas.style.backgroundColor = '#000000';
            now.fbImageText.style.opacity = 0;
          }
        }
        now.bgTransition = true;
      }
      var state = _states[sname]; 
      var interruptionModes = ['accelerate', 'invert', 'mixed', 'ignore'];
      if (interruptionModes.indexOf(interruptionMode) < 0) {
        interruptionMode = 'accelerate';
      }
      // if (now.bgTransition) {
      //   interruptionMode = 'ignore';
      // }
      if (state && state.type === 'background' && currentState.type !== 'background') {
        now.renderState = currentState;
      }
      if (!forced && !now.bgTransition) {
        var makeDeferred = function() {
          now.deferredState = state;
          now.deferredDuration = typeof duration === 'number' ? duration : config.transitionDuration;
          now.deferredEasing = easings['easeOutQuad'] || easings[config.defaultEasingName];
          now.deferredTransition = transition || Object.keys(transitions)[0];
        };
        if (now.transitionActive) {
          if (state.type !== 'background') {
            if (now.inverse && state === nextState) {
              lines.invert();
              return;
            }
            if (!now.inverse && state === currentState) {
              lines.invert();
              return;
            }
          }
          if (interruptionMode !== 'ignore' && (state !== currentState && state !== nextState || state.type === 'background')) {
            if (interruptionMode === 'invert') {
              lines.invert();
              return;
            } else {
              var nowT = new Date().getTime();
              var acceleration = 2;
              now.transitionFinishT = nowT + (now.transitionFinishT - nowT) / acceleration;
              now.transitionStartT = nowT - (nowT - now.transitionStartT) / acceleration;
              makeDeferred();
              return;
            }
          }
        }
        if (state === currentState) {
          return;
        }
        if (now.transitionActive || !state || typeof fsm[type] !== 'function') {
          console.warn('LineEngine: cannot make a transition to ' + sname);
          return;
        }
        if (state && state.dHue) {
          lines.setHue(state);
        }
      } else {
        now.animation = null;
      }
      // if (!transition) {
      //   transition = now.transition || Object.keys(transitions)[0];
      // } else if (!transitions.hasOwnProperty(transition)) {
      //   console.warn('LineEngine: cannot make a transition to ' + sname + '; ' + transition + ' doesn\'t exist');
      //   return;
      // }
      if (now.transitionActive) {
        if (fsm.transition && fsm.transition.cancel) {
          fsm.transition.cancel();
        }
      }
      if (params.onComplete) {
        now.onCompleteTransition = params.onComplete;
      }
      lines.selectBgVariation(state);
      now.transition = transition;
      nextState = state;
      now.prevState = currentState;
      now.transitionActive = true;
      now.easing = easings[easing] || easings[config.defaultEasingName];
      now.transitionK = 0;
      now.transitionStartT = new Date().getTime();
      now.duration = typeof duration === 'number' ? duration : config.transitionDuration;
      now.transitionFinishT = now.transitionStartT + now.duration;
      fsm[type]();
    },
    selectBgVariation: function(state) {
      if (!state.backgroundVariations) return;
      if (state.backgroundVariations === 1) {
        state.textures['bg_gradient'] = state.textures['bg_gradient_0'];
        return;
      }
      if (typeof state.currentBgIndex !== 'undefined') {
        var newIndex;
        do {
          newIndex = Math.floor(Math.random() * state.backgroundVariations);
        } while (state.currentBgIndex === newIndex);
        state.currentBgIndex = newIndex;
      } else {
        state.currentBgIndex = Math.floor(Math.random() * state.backgroundVariations);
      }
      return state.textures['bg_gradient_' + state.currentBgIndex];
      // state.textures['bg_gradient'] = state.textures['bg_gradient_' + state.currentBgIndex];
    },
    invert: function() {
      if (now.dissolving) return;
      if (!now.transitionStartT) {
        console.warn('LineEngine: cannot invert transition');
        return;
      }
      if (now.transitionActive) {
        var type = 'to-' + (now.inverse ? nextState.name : currentState.name);
        fsm.transition();
        now.inverse = !now.inverse;
        var nowT = new Date().getTime();
        var leftT = nowT - now.transitionStartT;
        now.transitionStartT = nowT - (now.transitionFinishT - nowT);
        now.transitionFinishT = nowT + leftT;
        type = type.replace('-', '').toLowerCase();
        fsm[type]();
      } else {
        var type = 'to-' + now.prevState.name;
        lines.transition({
          type: type,
          duration: now.duration,
          transition: now.transition,
          easing: now.easing
        });
      }
    },
    setState: function(stateName) {
      now.animation = null;
      now.needsUpdate = true;
      now.dissolving = false;
      now.dissolve = 0;
      now.bgDissolve = 0;
      now.opacity = 1;
      now.bgOpacity = 1;
      now.introDone = true;
      now.dy = 0;
      if (stateName === 'work' || stateName === 'about') {
        now.introDone = false;
        now.bgDissolve = 1;
        if (stateName === 'work') {
          now.bgColor = new THREE.Color(0x000000);
        } else {
          now.bgColor = new THREE.Color(0xffffff);
        }
        now.needsPause = true;
      }
      if (_states.hasOwnProperty(stateName)) {
        lines.transition({
          type: 'to-' + stateName,
          duration: 0, 
          forced: true
        });
      }
      now.dxTarget = now.dx = 0;
      now.hoverImpactTarget = now.hoverImpact = 0;
      if (now.renderVersion === 4) {
        lines.canvas.style.opacity = 1;
      }
    },
    setScrollPosition: function(params) {
      if (now.dissolving) return;
      var val = params.value|| 0;
      var scrollHeight = params.scrollHeight || 0;
      if (now.renderVersion < 4)  {
        var positions = lines.findWorldPositions(scrollHeight, new THREE.Vector2(0, 0));
        var dy = positions[0].y - positions[1].y;
        now.dy = val;
        now.dyWorld = val * dy;
      } else {
        now.dyWorld = scrollHeight * val;
      }
    },
    mousedown: function(params) {
      if (now.dissolving) return;
      params = params || {};
      var duration = params.duration || 0;
      var toWork = params.type === 'to-work';
      var toAbout = params.type === 'to-about';
      var toMing = params.type === 'to-ming';
      var fromState = params.from;

      if (now.renderVersion === 4) {
        if (toWork) {
          config.canvas.style.backgroundImage = '';
          config.canvas.style.backgroundColor = '#000000';
          now.fbImageText.style.opacity = 0;
        }
        if (toAbout) {
          config.canvas.style.backgroundImage = '';
          config.canvas.style.backgroundColor = '#ffffff';
          now.fbImageText.style.opacity = 0;
        }
        if (toMing) {
          config.canvas.style.backgroundImage = 'url(' + now.fbImageUrl + ')';
          now.fbImageText.style.opacity = 1;
        }
        return;
      }

      // if ((fromState === 'about' || fromState === 'work')) {
      if (!now.introShowed && now.renderVersion === 1 && (fromState === 'about' || fromState === 'work')) {
        if (now.renderVersion === 1) {
          now.bgDissolve = 0;
          now.opacity = 1;
          now.bgOpacity = 1;
          now.bgTransition = false;
          lines.animation('ming_1');
        }
        return;
      }

      now.onDissolveComplete = params.onComplete || function() {};
      var nowT = new Date().getTime();
      now.dissolveStartT = nowT;
      now.dissolveFinishT = nowT + duration;
      now.opacity = 1;
      now.bgTransition = false;
      if (toWork || toAbout) {
        now.dyStart = now.dy || 0;
        if (fromState === 'about' || fromState === 'work') {
          if (toAbout) {
            now.bgColor0 = new THREE.Color(0x000000);
            now.bgColor1 = new THREE.Color(0xffffff);
          }
          if (toWork) {
            now.bgColor0 = new THREE.Color(0xffffff);
            now.bgColor1 = new THREE.Color(0x000000);
          }
          // now.bgTransition = true;
        } else {
          if (toAbout) {
            now.bgColor = new THREE.Color(0xffffff);
          }
          if (toWork) {
            now.bgColor = new THREE.Color(0x000000);
          }
        }
        now.dissolveInvert = false;
      }
      if (!duration) {
        // now.dy = 0;
      }
      if (toMing) {
        now.hoverImpactTarget = now.hoverImpact = 0;
        now.dxTarget = now.dx = 0;
        if (fromState === 'work') {
          now.bgColor = new THREE.Color(0x000000);
        } else {
          now.bgColor = new THREE.Color(0xffffff);
        }
        now.dissolveInvert = true;
      }
      now.dissolving = true;
      if (now.renderVersion === 4) {
        lines.canvas.style.opacity = 1;
        now.dissolveFinishT = nowT;
      }
      now.animation = null;
      now.introDone = true;
    },
    findWorldPositions: function(size, pos) {
      var vector = new THREE.Vector3();
      var positions = [];
      vector.set(
          pos.x / now.w * 2 - 1,
          pos.y / now.h * 2 - 1,
          0.5 );
      vector.unproject( camera );
      var dir = vector.sub( camera.position ).normalize();
      var distance = (0.5 - camera.position.z) / dir.z;
      var p = camera.position.clone().add( dir.multiplyScalar( distance ) );
      positions.push(p);
      vector.set(
          (pos.x + size) / now.w * 2 - 1,
          (pos.y + size) / now.h * 2 - 1,
          0.5 );
      vector.unproject( camera );
      var dir = vector.sub( camera.position ).normalize();
      var distance = (0.5 - camera.position.z) / dir.z;
      var p = camera.position.clone().add( dir.multiplyScalar( distance ) );
      positions.push(p);
      return positions;
    },
    mouseover: function(params) {
      if (now.dissolving) return;
      params = params || {};
      now.dxTarget = params.xShift || 0;
      now.hoverImpactTarget = 1;
    },
    mouseout: function() {
      if (now.dissolving) return;
      now.dxTarget = 0;
      now.hoverImpactTarget = 0;
    },
    animations: {
      ming_1: [
        {
          duration: 250,
          easing: 'easeInOutCubic',
          opacity: 0,
          fboSpeed: 2,
          fov: 50,
          zNoise: 1,
          cameraPos: new THREE.Vector3(
            0.55,
            0.27,
            0.5
          ),
          lookAt: new THREE.Vector3(
            0.49,
            0.8,
            0.5
          ),
          up: new THREE.Vector3(0.3, 0.9, 1).normalize()
        },
        {
          duration: 1,
          easing: 'easeInSine',
          opacity: 1,
          fov: 50,
          zNoise: 0,
          cameraPos: new THREE.Vector3(
            0.5,
            0.5,
            1.33
          ),
          lookAt: new THREE.Vector3(
            0.5,
            0.5,
            -0.3
          ),
          up: new THREE.Vector3(0, 1, 0)
        },
      ]
    },
    animation: function(name) {
      if (!lines.animations[name]) return;
      now.animation = name;
      lines.processAnimation(name);
    },
    setHue: function(state) {
      var dHues = state.dHues;
      var _dHues = [];
      if (dHues.length > 1) {
        for (var i = 0; i < dHues.length; i++) {
          if (state.prevHue && (dHues[i] !== state.prevHue)) {
            _dHues.push(dHues[i]);
          } 
          if (!state.prevHue) {
            _dHues.push(dHues[i]);
          }
        }
        var ind = Math.floor(Math.random() * _dHues.length);
        state.dHue = _dHues[ind];
      } else {
        state.dHue = dHues[0];
      }
      state.prevHue = state.dHue;
    },
    initEvents: function() {
      var eventNames = ['AnimateInStart', 'AnimateInEnd', 'AnimateUpdate', 'AnimateOutStart', 'AnimateOutEnd'];
      for (var i = 0; i < eventNames.length; i++) {
        var name = eventNames[i];
        var event = document.createEvent('Event');
        event.initEvent(name, false, true);
        domEvents[name] = event;
      }
    },
    transitionProgress: function() {
      return now.transitionK;
    },
    resize: function(w, h) {
      now.w = w || parseInt(config.canvas.clientWidth, 10);
      now.h = h || parseInt(config.canvas.clientHeight, 10);
      if (now.renderVersion < 4) {
        camera.aspect = now.w / now.h;
        camera.updateProjectionMatrix();
        var pixelRatio = renderer.getPixelRatio();
        var sizeMultiplier = pixelRatio > 1 ? 1 : 1.;
        renderer.setSize(now.w * sizeMultiplier, now.h * sizeMultiplier);
        renderer.domElement.style.width = now.w + 'px';
        renderer.domElement.style.height = now.h + 'px';
        if (now.renderVersion === 3) {
          if (now.isInq) {

          } else {          
            var thknss = 0.023 * (window.devicePixelRatio ? 1 / window.devicePixelRatio : 1);
            thknss = 2;
            var rv3Side = 2048;
            var pSize = 7;
            now.opacityMul = 1.3;
            if (now.h <= 750 || isTouchDevice) {
              rv3Side = 1024;
              pSize = 4;
              now.opacityMul = 1.3;
            }
            linesMesh.material.uniforms['uPSize'].value = pSize;
            if (rv3LinesFbo.width !== rv3Side) {
              rv3LinesFbo.setSize(rv3Side, rv3Side);
              for (var i = 0; i < 2; i++) {
                rv3LinesFbos[i].setSize(rv3Side, rv3Side);
              }
            }
            now.currentRV3FboIndex = 0;
          }
        }
      } else if (now.isInq) {

      } else if (now.fbImageText) {
        var tw = now.fbImageText.width;
        var th = now.fbImageText.height;
        var ta = tw / th;
        var hm = 0.9;
        th = now.h * hm;
        tw = th * ta;
        if (tw > now.w * 0.8) {
          tw = now.w * 0.8;
          th = tw / ta;
        }
        now.fbImageText.style.width = tw + 'px';
        now.fbImageText.style.height = th + 'px';
        now.fbImageText.style.top = now.h / 2 + 'px';
        now.fbImageText.style.left = now.w / 2 + 'px';
        now.fbImageText.style.marginLeft = -tw / 2 + 'px';
        now.fbImageText.style.marginTop = -th / 2 + 'px';
      }
      now.resized = true;
    },
    setPointerPosition: function(x, y) {
      now.x = x;
      now.y = y;
    },
    pause: function() {
      now.active = false;
    },
    play: function() {
      now.active = true;
    },
    dealloc: function() {
      renderer.dispose();
      cancelAnimationFrame(rafInstance);
    },
    screenshot: function() {
      var img = renderer.domElement.toDataURL("image/png");
      document.body.innerHTML ='<img src="'+img+'"/>';
    }
  };

  lines.init();
  lines.states = _states;
  delete lines.init;
  delete lines.initEvents;
  return lines;
};

function smoothstep (min, max, value) {
  var x = Math.max(0, Math.min(1, (value-min)/(max-min)));
  return x*x*(3 - 2*x);
}

function clamp(num, min, max) {
  return num < min ? min : num > max ? max : num;
}

function getImageData( image, w, h ) {
    var canvas = document.createElement( 'canvas' );
    canvas.width = image.width;
    canvas.height = image.height;
    var context = canvas.getContext( '2d' );
    context.drawImage( image, 0, 0, image.width, image.height, 0, 0, w, h );
    return context.getImageData( 0, 0, w, h );
}

// DEBUG
// function toDataURL( w, h ) {
//     var canvas = document.createElement( 'canvas' );
//     canvas.width = w;
//     canvas.height = h;
//     var context = canvas.getContext( '2d' );
//     context.fillStyle="#000000";
//     context.globalAlpha=0;
//     context.fillRect(0,0,w,h);
//     document.body.innerHTML = '';
//     document.body.appendChild(canvas);
//     console.log(canvas.toDataURL());
// }

module.exports = {
  setup: setup,
  loadStates: loadStates,
  create: createLines,
  webgl: webgl,
  renderVersion: renderVersion,
};

