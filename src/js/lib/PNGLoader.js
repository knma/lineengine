var PngImage = require('./pngtoy.min');

THREE.XHRPNGLoader = function ( manager ) {

  this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

};

THREE.XHRPNGLoader.prototype = {

  constructor: THREE.XHRPNGLoader,

  load: function ( url, onLoad, onProgress, onError ) {

    if ( this.path !== undefined ) url = this.path + url;

    var scope = this;

    var request = new XMLHttpRequest();
    request.overrideMimeType( 'text/plain' );
    request.open( 'GET', url, true );

    request.addEventListener( 'load', function ( event ) {

      var response = event.target.response;

      if ( this.status === 200 ) {

        var img = new PngImage();
        img.onload = function() {
          var pngtoy = img.pngtoy;
          var dataObj = pngtoy.decode().then(function(results) {
              var buffer = new Uint16Array(results.bitmap);
              for(var i = 0, j; i < buffer.length; i++) {
                j = buffer[i];
                buffer[i] = ((j & 0xff) << 8) | ((j & 0xff00) >>> 8); // needed to swap bytes for correct unsigned integer values  
              }
              if ( onLoad ) { onLoad({
                  buffer: buffer,
                  width: img.width,
                  height: img.height
                });
              }
              scope.manager.itemEnd( url );
          });
        };
        img.src = url + '?nocache';
        window.fix_img_cache = window.fix_img_cache || []; window.fix_img_cache.push(img);

      } else if ( this.status === 0 ) {

        console.warn( 'THREE.XHRPNGLoader: HTTP Status 0 received.' );

        if ( onLoad ) onLoad( response );

        scope.manager.itemEnd( url );

      } else {

        if ( onError ) onError( event );

        scope.manager.itemError( url );

      }

    }, false );

    if ( onProgress !== undefined ) {

      request.addEventListener( 'progress', function ( event ) {

        onProgress( event );

      }, false );

    }

    request.addEventListener( 'error', function ( event ) {

      if ( onError ) onError( event );

      scope.manager.itemError( url );

    }, false );

    if ( this.responseType !== undefined ) request.responseType = this.responseType;
    if ( this.withCredentials !== undefined ) request.withCredentials = this.withCredentials;

    request.send( null );

    scope.manager.itemStart( url );

    return request;

  },

  setPath: function ( value ) {

    this.path = value;

  },

  setResponseType: function ( value ) {

    this.responseType = value;

  },

  setWithCredentials: function ( value ) {

    this.withCredentials = value;

  }

};

